// Fill out your copyright notice in the Description page of Project Settings.
#include "Collider.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/InputComponent.h"


// Sets default values
ACollider::ACollider()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// ======================== MY ADDED ITEMS (unreal default's above) ========================
	// ======================================== Create Static Mesh Component (+collision)
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("rootComponent"));
	sphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("sphereComponent"));
	sphereComp->SetupAttachment(GetRootComponent());

	sphereComp->InitSphereRadius(40.f);
	sphereComp->SetCollisionProfileName("Pawn");

	//
	meshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("meshComponent"));
	meshComp->SetupAttachment(GetRootComponent());

	// Note: This SHOULD NOT be hardcoded in code. Written here only as example.
	ConstructorHelpers::FObjectFinder<UStaticMesh> meshAsset(
		TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere'"));
	if (meshAsset.Succeeded())
	{
		meshComp->SetStaticMesh(meshAsset.Object);
		meshComp->SetRelativeLocation(FVector(0.f, 0.f, -40.f)); // Hardcode...
		meshComp->SetWorldScale3D(FVector(0.8f)); // Hardcode...
	}

	// ======================================== Create and setup Camera
	springArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("springArmComponent"));
	springArmComp->SetupAttachment(GetRootComponent());
	springArmComp->RelativeRotation = FRotator(-45.f, 0.f, 0.f);
	springArmComp->TargetArmLength = 400.f;
	springArmComp->bEnableCameraLag = true;
	springArmComp->CameraLagSpeed = 3.0f;

	cameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("cameraComponent"));
	cameraComp->SetupAttachment(springArmComp, USpringArmComponent::SocketName);

	// ======================================== Pawn Movement (+ Camera movement)
	// Player0 - it's the ONLY player in single game (first in multiplayer)
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	movementComp = CreateDefaultSubobject<UColliderMovementComponent>(TEXT("movementComponent"));
	movementComp->UpdatedComponent = RootComponent; // We set this, because this is the component we want to MOVE
	speed = 1;

	cameraInput = FVector2D(0.f);
}

// Called when the game starts or when spawned
void ACollider::BeginPlay()
{
	Super::BeginPlay();
}

// ======================== MY ADDED ITEMS (unreal default's above) ========================
//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// ======================================== Pawn Movement (+ Camera movement)
// Called every frame
void ACollider::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Move head up/down
	FRotator newRotation = GetActorRotation();
	newRotation.Yaw += cameraInput.X;
	SetActorRotation(newRotation);

	// Move head left/right
	FRotator newSpringArmRotation = springArmComp->GetComponentRotation();
	newSpringArmRotation.Pitch += cameraInput.Y;
	// Below fix/prevent for camera to set wrong angle
	newSpringArmRotation.Pitch = FMath::Clamp(newSpringArmRotation.Pitch, -80.f, -15.f);
	springArmComp->SetWorldRotation(newSpringArmRotation);
}

UPawnMovementComponent* ACollider::GetMovementComponent() const
{
	return movementComp;
}

// Called to bind functionality to input
void ACollider::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ACollider::moveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ACollider::moveRight);

	PlayerInputComponent->BindAxis(TEXT("CameraPitch"), this, &ACollider::pitchCamera);
	PlayerInputComponent->BindAxis(TEXT("CameraYaw"), this, &ACollider::yawCamera);
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
// ======================================== Pawn Movement
void ACollider::moveForward(float value)
{
	FVector forward = GetActorForwardVector();
	//AddMovementInput(forward, value);
	if (movementComp)
	{
		// TODO: fix speed (it doesn't affect anything) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		movementComp->AddInputVector(forward * value * speed);
	}
}

void ACollider::moveRight(float value)
{
	FVector right = GetActorRightVector();
	//AddMovementInput(right, value);
	if (movementComp)
	{
		// TODO: fix speed (it doesn't affect anything) <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		movementComp->AddInputVector(right * value * speed);
	}
}

void ACollider::pitchCamera(float axisValue)
{
	cameraInput.Y = axisValue;
}

void ACollider::yawCamera(float axisValue)
{
	cameraInput.X = axisValue;
}
