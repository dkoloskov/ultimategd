// Fill out your copyright notice in the Description page of Project Settings.
#include "MainCharacterAnimInstance.h"
#include "GameFramework/PawnMovementComponent.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
void UMainCharacterAnimInstance::NativeInitializeAnimation()
{
	if (pawn == nullptr)
	{
		pawn = TryGetPawnOwner();
		if (pawn)
		{
			mainCharacter = Cast<AMainCharacter>(pawn);
		}
	}
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
void UMainCharacterAnimInstance::updateAnimationProperties()
{
	if (pawn == nullptr || mainCharacter == nullptr)
	{
		return;
	}

	/*FVector speed = pawn->GetVelocity();
	movementSpeed = speed.Size2D(); // We need speed only in X|Y directions*/
	
	inAir = pawn->GetMovementComponent()->IsFalling();
}
