// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/SphereComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "ColliderMovementComponent.h"
#include "Collider.generated.h"


UCLASS()
class ULTIMATEGD_API ACollider : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACollider();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// ======================== MY ADDED ITEMS (unreal default's above) ========================
public:
	// ======================================== Create Static Mesh Component (+collision)
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent *meshComp;

	UPROPERTY(VisibleAnywhere)
	USphereComponent *sphereComp;

	// Note: FORCEINLINE == inline (���� �������, ��� ��������� ������������������. ��, �������� ������������ � �����������, ���� ����� ��� ����� ��� � �������� ������������������ � �����)
	FORCEINLINE UStaticMeshComponent* getMeshComponent() { return meshComp; }
	FORCEINLINE void setMeshComponent(UStaticMeshComponent *value) { meshComp = value; }

	FORCEINLINE USphereComponent* getSphereComponent() { return sphereComp; }
	FORCEINLINE void setSphereComponent(USphereComponent *value) { sphereComp = value; }

	// ======================================== Create and setup Camera
	UPROPERTY(VisibleAnywhere)
	UCameraComponent *cameraComp;

	UPROPERTY(VisibleAnywhere)
	USpringArmComponent *springArmComp; // <--- for control camera motion

	FORCEINLINE UCameraComponent* getCameraComponent() { return cameraComp; }
	FORCEINLINE void setCameraComponent(UCameraComponent *value) { cameraComp = value; }

	FORCEINLINE USpringArmComponent* getSpringArmComponent() { return springArmComp; }
	FORCEINLINE void setSpringArmComponent(USpringArmComponent *value) { springArmComp = value; }

	// ======================================== Pawn Movement (+ Camera movement)
	UPROPERTY(VisibleAnywhere)
	UColliderMovementComponent *movementComp;

	UPROPERTY(EditAnywhere, Category = "Movement")
	float speed;

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	virtual UPawnMovementComponent* GetMovementComponent() const override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

private:
	// ======================================== Pawn Movement (+ Camera movement)
	void moveForward(float value);
	void moveRight(float value);

	// Move around Y axis (rotate head up and down)
	void pitchCamera(float axisValue);
	// Move around Z axis (rotate head left and right)
	void yawCamera(float axisValue);

	FVector2D cameraInput;
};
