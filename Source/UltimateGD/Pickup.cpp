// Fill out your copyright notice in the Description page of Project Settings.
#include "Pickup.h"
#include "MainCharacter.h"
#include "Kismet/GameplayStatics.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
APickup::APickup()
{
	// coinCount = 1.f; // moved to BP
}

void APickup::onOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                             UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                             const FHitResult &SweepResult)
{
	Super::onOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	//UE_LOG(LogTemp, Warning, TEXT("APickup::onOverlapBegin()"));

	if (OtherActor)
	{
		AMainCharacter *mainCharacter = Cast<AMainCharacter>(OtherActor);
		if (mainCharacter)
		{
			onPickup(mainCharacter);
			//mainCharacter->increaseCoinsAmount(coinCount); // moved to BP

			// Show particles
			if (overlapParticlesSystem)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), overlapParticlesSystem, GetActorLocation(),
				                                         FRotator(0.f),
				                                         true, EPSCPoolMethod::None);
				// Note: EPSCPoolMethod - Enum ParticlesSystemComponent ...
			}
			// Play sound
			if (overlapSound)
			{
				UGameplayStatics::PlaySound2D(this, overlapSound, overlapSoundVolume);
			}

			// Debug stuff
			mainCharacter->pickupLocations.Push(GetActorLocation());

			Destroy();
		}
	}
}

void APickup::onOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                           UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
{
	Super::onOverlapEnd(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);
	//UE_LOG(LogTemp, Warning, TEXT("APickup::onOverlapEnd()"));
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
