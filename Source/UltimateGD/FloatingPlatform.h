// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "TimerManager.h"
#include "FloatingPlatform.generated.h"


UCLASS()
class ULTIMATEGD_API AFloatingPlatform : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AFloatingPlatform();

	/**
	 * Mesh for the platform
	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent *mesh;

	UPROPERTY(EditAnywhere, Category = "Platform")
	FVector startPoint;

	UPROPERTY(EditAnywhere, Category = "Platform", meta=(MakeEditWidget = "true"))
	FVector endPoint;
	// MakeEditWidget = "true" - this allow to: see and move endPoint as object in editor as "diamond wireframe (������ ��������)"
	// So I'm able to physically see point where platform will go.

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Platform")
	float interpSpeed;

	UPROPERTY(EditAnywhere, Category = "Platform")
	float flyDelay;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	float flyDistance;
	bool flyState; // Note: original var. name in v. lesson: "bInterping"

	void setFlyDelayTimer();
	// Delay between platform fly
	FTimerHandle flyDelayHandle; // NOT Handler, Delegate for timer
	// Note: original var. name in v. lesson: "interpTimer"

	void toggleFlyState();

	void swapStartAndEndPoints(FVector &v1, FVector &v2);
	// Note: original var. name in v. lesson: "SwapVectors"
};
