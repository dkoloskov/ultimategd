// Fill out your copyright notice in the Description page of Project Settings.
#include "MainPlayerController.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// ======================================== Enemy HUD
void AMainPlayerController::displayEnemyHealthBar()
{
	if (enemyHealthBar)
	{
		updateEnemyHealthBarPosition();

		enemyHealthBarVisible = true;
		enemyHealthBar->SetVisibility(ESlateVisibility::Visible);
	}
}

void AMainPlayerController::hideEnemyHealthBar()
{
	if (enemyHealthBar)
	{
		enemyHealthBarVisible = false;
		enemyHealthBar->SetVisibility(ESlateVisibility::Hidden);
	}
}

// ======================================== Pause Menu
void AMainPlayerController::displayPauseMenu_Implementation() // default implementation. Also will be overriden in BP
{
	if (pauseMenu)
	{
		pauseMenuVisible = true;
		pauseMenu->SetVisibility(ESlateVisibility::Visible);

		// ======================================== Set UI focus
		// Set focus to UI and game (default only game)
		FInputModeGameAndUI inputMode;
		SetInputMode(inputMode);
		// Note: input mode for UI only will disable keyboard events

		bShowMouseCursor = true;

		// ======================================== Freeze world: ON
		// UGameplayStatics::SetGamePaused(GetWorld(), true);
		UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 0);
		// Note: values between 0-1 will make slow motion effect
	}
}

void AMainPlayerController::hidePauseMenu_Implementation() // default implementation. Also will be overriden in BP
{
	if (pauseMenu)
	{
		//pauseMenu->SetVisibility(ESlateVisibility::Hidden); // <--- Will set this in BP
		pauseMenuVisible = false;
		
		// ======================================== Set UI focus
		// Set focus to game (default)
		FInputModeGameOnly inputMode;
		SetInputMode(inputMode);

		bShowMouseCursor = false;

		// ======================================== Freeze world: OFF
		// UGameplayStatics::SetGamePaused(GetWorld(), false); // This doesn't work for some reason
		UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 1);
	}
}

void AMainPlayerController::togglePauseMenu()
{
	if (pauseMenuVisible)
		hidePauseMenu();
	else
		displayPauseMenu();
}

// ======================================== Common
void AMainPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (enemyHealthBarVisible)
		updateEnemyHealthBarPosition();
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
void AMainPlayerController::BeginPlay()
{
	Super::BeginPlay();

	// ======================================== Player HUD
	if (HUDOverlayAsset)
	{
		// CreateWidget() - like CreateDefaultSubobject() but for Widgets
		HUDOverlay = CreateWidget<UUserWidget>(this, HUDOverlayAsset);

		HUDOverlay->AddToViewport();
		HUDOverlay->SetVisibility(ESlateVisibility::Visible);
		// I think this line is irrelevant (done like in v. lesson)
	}

	// ======================================== Enemy HUD
	if (enemyHealthBarAsset)
	{
		// CreateWidget() - like CreateDefaultSubobject() but for Widgets
		enemyHealthBar = CreateWidget<UUserWidget>(this, enemyHealthBarAsset);
		if (enemyHealthBar)
		{
			enemyHealthBar->AddToViewport();

			FVector2D alignment(0.f, 0.f);
			enemyHealthBar->SetAlignmentInViewport(alignment);

			hideEnemyHealthBar();
		}
	}

	// ======================================== Pause Menu
	if (pauseMenuAsset)
	{
		// CreateWidget() - like CreateDefaultSubobject() but for Widgets
		pauseMenu = CreateWidget<UUserWidget>(this, pauseMenuAsset);
		if (pauseMenu)
		{
			pauseMenu->AddToViewport();

			FVector2D alignment(0.f, 0.f);
			pauseMenu->SetAlignmentInViewport(alignment);

			//
			pauseMenu->SetVisibility(ESlateVisibility::Hidden); // <--- Will set this in BP
			pauseMenuVisible = false;
		}
	}
}

// ======================================== Enemy HUD
void AMainPlayerController::updateEnemyHealthBarPosition()
{
	if (enemyHealthBar)
	{
		FVector2D positionInViewport; // <--- this variable will be changed in function below
		ProjectWorldLocationToScreen(enemyLocation, positionInViewport);
		positionInViewport.Y -= 85; // to make health bar above enemy
		positionInViewport.X -= 85; // to make health bar above enemy

		FVector2D sizeInViewport = FVector2D(300.f, 25.f);

		enemyHealthBar->SetPositionInViewport(positionInViewport);
		enemyHealthBar->SetDesiredSizeInViewport(sizeInViewport); // I think this should be in BeginPlay()
	}
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
