// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"
#include "Creature.generated.h"

/**
 * In lessons this class called: "Critter" - ��������, �����, ��������
 */
UCLASS()
class ULTIMATEGD_API ACreature : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACreature();

	// ======================================== Create Static Mesh Component
	UPROPERTY(VisibleAnywhere)
	USkeletalMeshComponent *meshComp;

	// ======================================== Create and setup Camera
	UPROPERTY(VisibleAnywhere)
	UCameraComponent *cameraComp;

	// ======================================== Pawn Movement
	UPROPERTY(EditAnywhere, Category="Pawn Movement")
	float maxSpeed;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

protected:
	// ========================================
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// ======================================== Pawn Movement
	void moveForward(float value);
	void moveRight(float value);

	FVector currentVelocity;
};
