// Fill out your copyright notice in the Description page of Project Settings.
#include "Item.h"
#include "Engine/World.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// Sets default values
AItem::AItem()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	collisionVolume = CreateDefaultSubobject<USphereComponent>(TEXT("collisionVolume"));
	RootComponent = collisionVolume;

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("mesh"));
	mesh->SetupAttachment(GetRootComponent());

	idleParticles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("idleParticles"));
	idleParticles->SetupAttachment(GetRootComponent());

	//
	overlapSoundVolume = 1.f;
	rotate = false;
	rotationRate = 45.f;
}

// Called every frame
void AItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (rotate)
	{
		FRotator rotator = GetActorRotation();
		rotator.Yaw += DeltaTime * rotationRate;
		SetActorRotation(rotator);
	}
}

void AItem::onOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor, UPrimitiveComponent *OtherComp,
                           int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("Item::onOverlapBegin()"));
}

void AItem::onOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor, UPrimitiveComponent *OtherComp,
                         int32 OtherBodyIndex)
{
	//UE_LOG(LogTemp, Warning, TEXT("Item::onOverlapEnd()"));
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();

	collisionVolume->OnComponentBeginOverlap.AddDynamic(this, &AItem::onOverlapBegin);
	collisionVolume->OnComponentEndOverlap.AddDynamic(this, &AItem::onOverlapEnd);
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
