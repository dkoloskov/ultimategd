// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Components/BillboardComponent.h"
#include "LevelTransitionVolume.generated.h"


UCLASS()
class ULTIMATEGD_API ALevelTransitionVolume : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ALevelTransitionVolume();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Transition")
	UBoxComponent *transitionVolume;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Transition")
	FName transitionLevelName;

	UBillboardComponent *bilboard;

	// ======================================== Common
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//
	UFUNCTION() // <--- this is MUST, for overlap functions
	virtual void onOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                            UPrimitiveComponent *OtherComp,
	                            int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION() // <--- this is MUST, for overlap functions
	virtual void onOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                          UPrimitiveComponent *OtherComp,
	                          int32 OtherBodyIndex);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
