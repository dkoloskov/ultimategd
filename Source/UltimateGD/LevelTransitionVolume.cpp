// Fill out your copyright notice in the Description page of Project Settings.
#include "LevelTransitionVolume.h"
#include "MainCharacter.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// Sets default values
ALevelTransitionVolume::ALevelTransitionVolume()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	transitionVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("transitionVolume"));
	SetRootComponent(transitionVolume);

	bilboard = CreateDefaultSubobject<UBillboardComponent>(TEXT("bilboard"));
	bilboard->SetupAttachment(GetRootComponent());

	transitionLevelName = "SunTemple"; // Good old hardcode
}

// ======================================== Common
// Called every frame
void ALevelTransitionVolume::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//
void ALevelTransitionVolume::onOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                            UPrimitiveComponent *OtherComp,
                                            int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("ALevelTransitionVolume::onOverlapBegin()"));
	if (OtherActor)
	{
		AMainCharacter *mainCharacter = Cast<AMainCharacter>(OtherActor);
		if (mainCharacter)
		{
			mainCharacter->switchLevel(transitionLevelName);
		}
	}
}

void ALevelTransitionVolume::onOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                          UPrimitiveComponent *OtherComp,
                                          int32 OtherBodyIndex)
{
	//UE_LOG(LogTemp, Warning, TEXT("ALevelTransitionVolume::onOverlapEnd()"));
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
// ======================================== Common
// Called when the game starts or when spawned
void ALevelTransitionVolume::BeginPlay()
{
	Super::BeginPlay();

	transitionVolume->OnComponentBeginOverlap.AddDynamic(this, &ALevelTransitionVolume::onOverlapBegin);
	transitionVolume->OnComponentEndOverlap.AddDynamic(this, &ALevelTransitionVolume::onOverlapEnd);
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
