// Fill out your copyright notice in the Description page of Project Settings.
#include "Explosive.h"
#include "MainCharacter.h"
#include "Enemy.h"
#include "Kismet/GameplayStatics.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
AExplosive::AExplosive()
{
	damage = 15.f;
}

void AExplosive::onOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                const FHitResult &SweepResult)
{
	Super::onOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	//UE_LOG(LogTemp, Warning, TEXT("Explosive::onOverlapBegin()"));

	if (OtherActor)
	{
		AMainCharacter *mainCharacter = Cast<AMainCharacter>(OtherActor);
		AEnemy *enemy = Cast<AEnemy>(OtherActor);
		if ((mainCharacter || enemy) && damageTypeClass)
		{
			UGameplayStatics::ApplyDamage(OtherActor, damage, nullptr, this, damageTypeClass);
			
			// Show particles
			if (overlapParticlesSystem)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), overlapParticlesSystem, GetActorLocation(),
				                                         FRotator(0.f),
				                                         true, EPSCPoolMethod::None);
				// Note: EPSCPoolMethod - Enum ParticlesSystemComponent ...
			}
			// Play sound
			if (overlapSound)
			{
				UGameplayStatics::PlaySound2D(this, overlapSound, overlapSoundVolume);
			}
			
			Destroy();
		}
	}
}

void AExplosive::onOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                              UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
{
	Super::onOverlapEnd(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);
	UE_LOG(LogTemp, Warning, TEXT("Explosive::onOverlapEnd()"));
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
