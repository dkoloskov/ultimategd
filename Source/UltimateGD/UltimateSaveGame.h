// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "UltimateSaveGame.generated.h"


USTRUCT(BlueprintType) // <----- Now this struct can be used in Blueprints
struct FCharacterStats
{
	GENERATED_BODY() // <----- Required to generate some boilerplate code, to participate in reflection system

	UPROPERTY(VisibleAnywhere, Category = "SaveGameData")
	float maxHealth;

	UPROPERTY(VisibleAnywhere, Category = "SaveGameData") // <--- can be any other custom category
	float health;

	UPROPERTY(VisibleAnywhere, Category = "SaveGameData")
	float maxStamina;

	UPROPERTY(VisibleAnywhere, Category = "SaveGameData")
	float stamina;

	UPROPERTY(VisibleAnywhere, Category = "SaveGameData")
	int32 coins;

	// ================================================== Below things not related to stats...
	UPROPERTY(VisibleAnywhere, Category = "SaveGameData")
	FVector location;

	UPROPERTY(VisibleAnywhere, Category = "SaveGameData")
	FRotator rotation;

	// ==================================================
	UPROPERTY(VisibleAnywhere, Category = "SaveGameData")
	FString weaponName;

	// ==================================================
	UPROPERTY(VisibleAnywhere, Category = "SaveGameData")
	FString levelName;
};


/**
 * 
 */
UCLASS()
class ULTIMATEGD_API UUltimateSaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	UUltimateSaveGame();

	UPROPERTY(VisibleAnywhere, Category = Basic) // <--- "Category = Basic" is MUST, not sure about other param
	FString playerName;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	uint32 userIndex; // slot for save the game

	UPROPERTY(VisibleAnywhere, Category = Basic)
	FCharacterStats characterStats;
};
