// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Floater.generated.h"

UCLASS()
class ULTIMATEGD_API AFloater : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AFloater();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// ======================== MY ADDED ITEMS (unreal default's above) ========================
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Actor Mesh Components")
	UStaticMeshComponent *staticMeshComp;
	// IMPORTANT Note: EditAnywhere - makes elements smaller in Editor... So it's better to use "VisibleAnywhere"

	// +++++++++++++++++++++++++ FVector examples (+ Property Specifiers) +++++++++++++++++++++++++
	// Location used by SetActorLocation() when BeginPlay() is called
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Floater Variables")
	FVector initialLocation;
	// EditInstanceOnly - ����� ������������� ������ Instance

	// Location of the Actor when dragged in from the editor
	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category= "Floater Variables")
	FVector placedLocation;
	// VisibleInstanceOnly - ����� ������ � Instance

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Floater Variables")
	FVector worldOrigin;
	// VisibleDefaultsOnly - ����� ������ ������ � BP, �� �� �� Instance

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Floater Variables")
	FVector initialDirection;
	// EditAnywhere - ����� ������������� ����� (��� � � ���� ����� ������������)

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Floater Variables")
	bool shouldFloat;

	// initializeFloaterLocations - actually is "setInitialLocation" flag (my note: which is better name)
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Floater Variables")
	bool initializeFloaterLocations;
	// EditDefaultsOnly - ����� ������������� ������ Editor, �� �� Instance
	// ------------------------- FVector examples (+ Property Specifiers) -------------------------

	// ======================================== Add Force and Torque
	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category= "Floater Variables")
	FVector initialForce;

	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = "Floater Variables")
	FVector initialTorque;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// ======================================== FMath::Sin (Floating wave)
	float runningTime;

	// Wave range multiplication
	UPROPERTY(EditAnywhere, Category= "Floater Variables | Wave Parameters")
	float waveAmplitude;

	// Time multiplication
	UPROPERTY(EditAnywhere, Category = "Floater Variables | Wave Parameters")
	float waveTimeStretch;
};
