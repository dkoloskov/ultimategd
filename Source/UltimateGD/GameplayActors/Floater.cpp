// Fill out your copyright notice in the Description page of Project Settings.
#include "Floater.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
AFloater::AFloater()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// ======================================== Create Static Mesh Component
	staticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CustomStaticMesh"));

	// ======================================== FVector examples
	initialLocation = FVector(.0f);
	placedLocation = FVector(.0f);
	worldOrigin = FVector(.0f, .0f, .0f);
	initialDirection = FVector(.0f, .0f, .0f);

	initializeFloaterLocations = false;
	shouldFloat = false;

	// ======================================== Add Force and Torque
	initialForce = FVector(2000000.0f, .0f, .0f);
	initialTorque = FVector(2000000.0f, .0f, .0f);

	// ======================================== FMath::Sin (Floating wave)
	runningTime = 0.f;
	waveAmplitude = waveTimeStretch = 1.f;
}

//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// Called every frame
void AFloater::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (shouldFloat)
	{
		FVector newLocation = GetActorLocation();
		newLocation.X += waveAmplitude * FMath::Sin(waveTimeStretch * runningTime);
		newLocation.Y += waveAmplitude * FMath::Cos(waveTimeStretch * runningTime);
		newLocation.Z += waveAmplitude * FMath::Sin(waveTimeStretch * runningTime);
		SetActorLocation(newLocation);

		runningTime += DeltaTime;

		/*FHitResult hitResult;
		// bSweep - is a "check collision" flag
		AddActorLocalOffset(initialDirection, true, &hitResult);

		FVector hitLocation = hitResult.Location;
		UE_LOG(LogTemp, Warning, TEXT("Hit Location: X = %f, Y = %f, Z = %f"), hitLocation.X, hitLocation.Y,
		       hitLocation.Z);*/
	}
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
// Called when the game starts or when spawned
void AFloater::BeginPlay()
{
	Super::BeginPlay();

	initialLocation.X = FMath::FRandRange(-500.f, 500.f);
	initialLocation.Y = FMath::FRandRange(-500.f, 500.f);
	initialLocation.Z = FMath::FRandRange(0.f, 500.f);
	//initialLocation *= 500.f;

	placedLocation = GetActorLocation();
	if (initializeFloaterLocations)
	{
		SetActorLocation(initialLocation);
	}

	/*staticMeshComp->AddForce(initialForce);
	staticMeshComp->AddTorqueInRadians(initialTorque);*/
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
