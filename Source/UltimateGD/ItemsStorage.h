// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ItemsStorage.generated.h"


UCLASS()
class ULTIMATEGD_API AItemsStorage : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AItemsStorage();

	UPROPERTY(EditDefaultsOnly, Category = "SaveData")
	TMap<FString, TSubclassOf<class AWeapon>> weaponMap;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
