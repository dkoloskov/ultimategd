// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "SpawnVolume.generated.h"


UCLASS()
class ULTIMATEGD_API ASpawnVolume : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASpawnVolume();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Spawning")
	UBoxComponent *spawningBox;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Spawning")
	TSubclassOf<AActor> actor_1;
	// AActor *pawnToSpawn;
	// Note: ������� ������ ����� ��������� (������ ����) ������-�� �� ��������� �������� � Editor ���������� �������.
	// ������ ������, � ��������. � ������ "TSubclassOf<...>" ��������� �������� ���������� �� ���� �++ � BP �������
	// ��� pawnToSpawn � Editor.

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Spawning")
	TSubclassOf<AActor> actor_2;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Spawning")
	TSubclassOf<AActor> actor_3;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Spawning")
	TSubclassOf<AActor> actor_4;

	TArray<TSubclassOf<AActor>> spawnArray; // <--- It will be better to make this UPROPERTY

	/**
	 * Get and return random point in spawn volume
	 */
	UFUNCTION(BlueprintPure, Category = "Spawning")
	FVector getSpawnPoint();
	// Note: BlueprintPure - only return value (input parameters not allowed)

	UFUNCTION(BlueprintPure, Category = "Spawning")
	TSubclassOf<AActor> getSpawnActor();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Spawning")
	void spawnActor(TSubclassOf<AActor> actorClass, const FVector &location);
	// Note: BlueprintNativeEvent - designed to be overridden by a BP, but also has a default C++ implementation
	// as "funtion name"_Implementation()

	// ======================================== Common
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// ======================================== Common
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
