// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"


/**
 * 
 */
UCLASS()
class ULTIMATEGD_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	// ======================================== Player HUD
	/**
	 * Reference to the UMG asset in the editor
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<UUserWidget> HUDOverlayAsset;
	// Note:
	// with "TSubclassOf<UUserWidget>" - we have type check during compilation, so wrong type can't be passed
	// we can use "UClass*" instead - in that case type check will be performed during runtime, which is good way to have bugs

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	UUserWidget *HUDOverlay;

	// ======================================== Enemy HUD
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<UUserWidget> enemyHealthBarAsset;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Widgets")
	UUserWidget *enemyHealthBar;

	//
	bool enemyHealthBarVisible;

	FVector enemyLocation;

	void displayEnemyHealthBar();
	void hideEnemyHealthBar(); // v. lessons name: removeEnemyHealthBar()

	// ======================================== Pause Menu
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<UUserWidget> pauseMenuAsset;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Widgets")
	UUserWidget* pauseMenu;

	bool pauseMenuVisible;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "HUD")
	void displayPauseMenu();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "HUD")
	void hidePauseMenu(); // v. lessons name: removePauseMenu()
	
	void togglePauseMenu(); // show/hide
	
	// ======================================== Common
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

	void updateEnemyHealthBarPosition();
};
