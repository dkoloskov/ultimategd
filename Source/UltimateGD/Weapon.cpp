// Fill out your copyright notice in the Description page of Project Settings.
#include "Weapon.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Kismet/GameplayStatics.h"
#include "Enemy.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
AWeapon::AWeapon()
{
	skeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("skeletalMesh"));
	skeletalMesh->SetupAttachment(GetRootComponent());

	//
	showParticles = false;

	weaponState = WeaponStateEnum::Pickup;

	// ======================================== Attack
	attackCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("combatCollision"));
	attackCollision->SetupAttachment(GetRootComponent());

	// ======================================== Weapon Stats
	damage = 25.f;

	// ======================================== Sounds
	equipSoundVolume = 1.f;
	swingSoundVolume = 1.f;
}

// ======================================== Equip
void AWeapon::becomeEquipped(AMainCharacter *mainCharacter)
{
	if (mainCharacter)
	{
		setWeaponInstigator(mainCharacter->GetController());

		// Make sure not to ZOOM when camera between player and weapon
		// Note: maybe it should be in BeginPlay() ???
		skeletalMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
		// Disable collision with character
		skeletalMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);

		// Maybe line below isn't required, but, we want to be sure that simulation is disabled for skeletalMesh.
		skeletalMesh->SetSimulatePhysics(false);

		const USkeletalMeshSocket *handSocket = mainCharacter->GetMesh()->GetSocketByName("RightHandSocket");
		if (handSocket)
		{
			handSocket->AttachActor(this, mainCharacter->GetMesh());
			// Also save reference of this weapon in mainCharacter
			mainCharacter->setEquippedWeapon(this);
			mainCharacter->setOverlappingItem(nullptr);

			//
			rotate = false;
			if (!showParticles)
			{
				idleParticles->Complete();
			}
		}

		//
		if (equipSound)
			UGameplayStatics::PlaySound2D(this, equipSound, equipSoundVolume);
	}
}

//
void AWeapon::onOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                             UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                             const FHitResult &SweepResult)
{
	Super::onOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	//UE_LOG(LogTemp, Warning, TEXT("AWeapon::onOverlapBegin()"));

	if (weaponState == WeaponStateEnum::Pickup && OtherActor)
	{
		AMainCharacter *mainCharacter = Cast<AMainCharacter>(OtherActor);
		if (mainCharacter)
		{
			mainCharacter->setOverlappingItem(this);
		}
	}
}

void AWeapon::onOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                           UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
{
	Super::onOverlapEnd(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);
	//UE_LOG(LogTemp, Warning, TEXT("AWeapon::onOverlapEnd()"));

	if (OtherActor)
	{
		AMainCharacter *mainCharacter = Cast<AMainCharacter>(OtherActor);
		if (mainCharacter)
			mainCharacter->setOverlappingItem(nullptr);
	}
}

// ======================================== Attack
void AWeapon::activateCollision()
{
	// Collision only for: overlap, raycast, etc. No physics collision
	attackCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AWeapon::deactivateCollision()
{
	attackCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AWeapon::playSwingSound()
{
	// Play weapon swing sound
	if (swingSound)
	{
		UGameplayStatics::PlaySound2D(this, swingSound, swingSoundVolume);
	}
}

void AWeapon::onAttackOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                   UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                   const FHitResult &SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("AWeapon::onCombatOverlapBegin(), bFromSweep: %i"), bFromSweep);

	if (OtherActor)
	{
		AEnemy *enemy = Cast<AEnemy>(OtherActor);
		if (enemy && damageTypeClass)
		{
			UGameplayStatics::ApplyDamage(enemy, damage, weaponInstigator, this, damageTypeClass);

			// ======================================== Alternative blood particles spawn functionality (v. lesson style)
			// Approach below will look better, but only for specific animations and specific size enemies
			/*const USkeletalMeshSocket *weaponSocket = skeletalMesh->GetSocketByName("WeaponSocket");
			if (weaponSocket)
			{
				FVector socketLocation = weaponSocket->GetSocketLocation(skeletalMesh);
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), enemy->hitParticles, socketLocation,
				                                         FRotator(0.f),
				                                         true, EPSCPoolMethod::None);
				// Note: EPSCPoolMethod - Enum ParticlesSystemComponent ...
			}*/
		}
	}
}

void AWeapon::onAttackOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                 UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
{
	//UE_LOG(LogTemp, Warning, TEXT("AWeapon::onCombatOverlapEnd()"));
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
// ======================================== Common
void AWeapon::BeginPlay()
{
	Super::BeginPlay();

	// ======================================== Attack
	attackCollision->OnComponentBeginOverlap.AddDynamic(this, &AWeapon::onAttackOverlapBegin);
	attackCollision->OnComponentEndOverlap.AddDynamic(this, &AWeapon::onAttackOverlapEnd);

	// Collision will be activated during attack animation (on "Notify")
	attackCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision); // will be activated during attack
	attackCollision->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	// Set collision response only for 1 specific chanel
	attackCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	attackCollision->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
