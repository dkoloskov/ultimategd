// Fill out your copyright notice in the Description page of Project Settings.
#include "MainCharacter.h"
#include "GameFramework/Controller.h"
#include "Engine/World.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Weapon.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Animation/AnimInstance.h"
#include "MainPlayerController.h"
#include "Enemy.h"
#include "UltimateSaveGame.h"
#include "ItemsStorage.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// Sets default values
AMainCharacter::AMainCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// ======================================== Create and setup Camera
	// Spring pulls camera towards the player if there's a collision
	cameraSpringComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("cameraSpringComponent"));
	cameraSpringComp->SetupAttachment(GetRootComponent());
	cameraSpringComp->TargetArmLength = 600.f; // Camera follows at this distance
	cameraSpringComp->bUsePawnControlRotation = true; // Rotate arm based on controller

	followCameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("followCameraComponent"));
	followCameraComp->SetupAttachment(cameraSpringComp, USpringArmComponent::SocketName);
	// Attach the camera to the end of the spring and let the spring adjust to match
	// the controller orientation
	followCameraComp->bUsePawnControlRotation = false;

	// ======================================== Movement + Camera movement
	// Camera turn rates for input
	cameraTurnRate = 65.f; // degrees per second
	cameraLookUpRate = 65.f; // degrees per second

	// Don't rotate character when controller rotates (so only camera will rotate)
	bUseControllerRotationYaw = false;
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;

	// Rotate character to face direction of movement (���� ���� ����� ��� �������� �� ����...)
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 1000.f, 0.0f); // Character rotation speed around Z axis
	GetCharacterMovement()->JumpZVelocity = 650.f; // Jump speed&height
	GetCharacterMovement()->AirControl = 0.2f; // Rate of moving char during jump

	// ======================================== ...
	// Set size for collision capsule
	// Note: not sure that I need to hardcode this
	GetCapsuleComponent()->InitCapsuleSize(35.f, 88.f);

	// ======================================== Player Stats
	maxHealth = 100.f;
	health = 65.f;

	maxStamina = 150.f;
	stamina = 120.f;

	coins = 0;

	// ======================================== Run/Sprint + Stamina
	runSpeed = 650.f;
	sprintSpeed = 950.f;

	staminaDrainRate = 25.f; // per second
	minSprintStamina = 50.f;

	movementState = MovementStateEnum::Normal;
	staminaState = StaminaStateEnum::Normal;

	shiftPressed = false;

	// ======================================== Attack
	attacking = false;

	FString arr[] = {TEXT("Attack_1"), TEXT("Attack_2")};
	attackNames.Append(arr, UE_ARRAY_COUNT(arr));

	attackAnimationSpeed.Add(attackNames[0], 2.2f);
	attackAnimationSpeed.Add(attackNames[1], 1.8f);

	// ======================================== Turn to enemy interpolation (Lock on target)
	interpToEnemy = false;
	interpToEnemySpeed = 15.f;

	// ======================================== Sounds
	hitSoundVolume = 1.f;

	// ======================================== Common
	lmbPressed = false;
}

// ======================================== Player Stats
void AMainCharacter::increaseHealth(float value)
{
	health += value;
	if (health > maxHealth)
		health = maxHealth;
}

void AMainCharacter::decreaseHealth(float value)
{
	health -= value;
	if (health <= 0)
	{
		stamina = 0; // Will look better, then full stamina when dead
		die();
	}
}

void AMainCharacter::increaseCoinsAmount(int32 value)
{
	coins += value;
}

// ======================================== Weapon/Item
void AMainCharacter::setEquippedWeapon(AWeapon *weapon)
{
	// Remove previous weapon in hand (if exist)
	if (equipedWeapon)
	{
		equipedWeapon->Destroy();
	}

	//
	equipedWeapon = weapon;

	// TODO: If Weapon::"overlap mehods" will NOT be used for attack enemies - REMOVE handling of these events in Weapon
}

// ======================================== Attack
void AMainCharacter::attack()
{
	attacking = true;
	setInterpToEnemy(true);

	UAnimInstance *animInstance = GetMesh()->GetAnimInstance();
	if (animInstance && combatMontage)
	{
		// Randomize attack type
		int32 attackNum = FMath::RandRange(0, 1);

		//
		// FString attackName = FString::Printf(TEXT("Attack_%d"), attackNum);
		FString attackName = attackNames[attackNum];
		float attackSpeed = attackAnimationSpeed[attackName];

		animInstance->Montage_Play(combatMontage, attackSpeed);
		animInstance->Montage_JumpToSection(*attackName, combatMontage);

		// UE_LOG(LogTemp, Error, TEXT("%s"), *attackName);
	}
}

// This methods will be called in ...Animation_BP, on "EndAttacking" Notify (Event)
void AMainCharacter::attackEnd()
{
	attacking = false;

	if (movementState != MovementStateEnum::Dead)
	{
		setInterpToEnemy(false); // because we don't want to "stick" to enemy when we don't attack

		// If mouse pressed, attack again
		if (lmbPressed)
		{
			//
			if (attackTarget)
			{
				attack();
			}
		}
	}
}

// ======================================== Take damage
float AMainCharacter::TakeDamage(float Damage, FDamageEvent const &DamageEvent, AController *EventInstigator,
                                 AActor *DamageCauser)
{
	decreaseHealth(Damage);

	if (hitParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), hitParticles, GetActorLocation(), FRotator(0.f), true,
		                                         EPSCPoolMethod::None);
		// Note: EPSCPoolMethod - Enum ParticlesSystemComponent ...
	}

	if (hitSound)
	{
		UGameplayStatics::PlaySound2D(this, hitSound, hitSoundVolume);
	}

	return Damage;
}

// ======================================== Death + Disappear
void AMainCharacter::die()
{
	if (movementState == MovementStateEnum::Dead)
		return;

	UAnimInstance *animInstance = GetMesh()->GetAnimInstance();
	if (animInstance && combatMontage)
	{
		animInstance->Montage_Play(combatMontage, 1.f);
		animInstance->Montage_JumpToSection(FName("Death"), combatMontage);
		// Note: I think this isn't the best place to store death animation (I mean why with attacks?)
	}

	//
	movementState = MovementStateEnum::Dead;

	// Disable collisions
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AMainCharacter::deathEnd()
{
	GetMesh()->bPauseAnims = true;
	GetMesh()->bNoSkeletonUpdate = true;

	//GetWorldTimerManager().SetTimer(disapearDelayHandle, this, &AEnemy::disappear, disappearDelay);
}

// ======================================== Other Global functionality (which shouldn't be here)
void AMainCharacter::switchLevel(FName levelName)
{
	UWorld *world = GetWorld();
	if (world)
	{
		FString currentLevelStr = GetWorld()->GetMapName();
		currentLevelStr.RemoveFromStart(GetWorld()->StreamingLevelsPrefix); // because map name has "instance" prefix
		FName currentLevelName(*currentLevelStr);
		
		//if (currentLevelStr != levelName.ToString()) // Alternative strings comparison FYI
		if (currentLevelName != levelName)
		{
			// TODO: save game state (weapon, stats, etc) when before load new level <----------
			// Note: can't done it with current v. lessons approach while 1 profile is used
			UGameplayStatics::OpenLevel(world, levelName);
		}
	}
}

void AMainCharacter::saveGame()
{
	UUltimateSaveGame *saveGameInstance = Cast<UUltimateSaveGame>(
		UGameplayStatics::CreateSaveGameObject(UUltimateSaveGame::StaticClass()));

	// Stats
	saveGameInstance->characterStats.maxHealth = maxHealth;
	saveGameInstance->characterStats.health = health;
	saveGameInstance->characterStats.maxStamina = maxStamina;
	saveGameInstance->characterStats.stamina = stamina;
	saveGameInstance->characterStats.coins = coins;
	
	// Position
	saveGameInstance->characterStats.location = GetActorLocation();
	saveGameInstance->characterStats.rotation = GetActorRotation();
	
	// Weapon
	if (equipedWeapon)
		saveGameInstance->characterStats.weaponName = equipedWeapon->name;
	
	// Level/Map
	FString mapName = GetWorld()->GetMapName();
	mapName.RemoveFromStart(GetWorld()->StreamingLevelsPrefix); // because map name has "instance" prefix
	saveGameInstance->characterStats.levelName = mapName;

	UGameplayStatics::SaveGameToSlot(saveGameInstance, saveGameInstance->playerName, saveGameInstance->userIndex);
}

void AMainCharacter::loadGame(bool setPosition)
{
	// We doing this only to get default name and slot, so NOT mandatory step
	UUltimateSaveGame *loadGameInstance = Cast<UUltimateSaveGame>(
		UGameplayStatics::CreateSaveGameObject(UUltimateSaveGame::StaticClass()));

	// Load game
	loadGameInstance = Cast<UUltimateSaveGame>(
		UGameplayStatics::LoadGameFromSlot(loadGameInstance->playerName, loadGameInstance->userIndex));

	// Level/Map
	if (!loadGameInstance->characterStats.levelName.IsEmpty())
	{
		FName levelName(*loadGameInstance->characterStats.levelName);
		switchLevel(levelName);
		// WARNING: functionality code below will NOT be called! Because of level switch and RE create MainCharacter
	}
	
	// Stats
	maxHealth = loadGameInstance->characterStats.maxHealth;
	health = loadGameInstance->characterStats.health;
	maxStamina = loadGameInstance->characterStats.maxStamina;
	stamina = loadGameInstance->characterStats.stamina;
	coins = loadGameInstance->characterStats.coins;
	
	// Position
	if (setPosition)
	{
		SetActorLocation(loadGameInstance->characterStats.location);
		SetActorRotation(loadGameInstance->characterStats.rotation);
	}
	
	// Weapon
	FString weaponName = loadGameInstance->characterStats.weaponName;
	if (weaponStorage && !(weaponName.IsEmpty()))
	{
		// I don't understand why are we doing this... It's just dumb to create this second time! (v. lesson approach)
		AItemsStorage *weapons = GetWorld()->SpawnActor<AItemsStorage>(weaponStorage);
		if (weapons)
		{
			TSubclassOf<AWeapon> weaponClass = weapons->weaponMap[weaponName];

			AWeapon *weapon = GetWorld()->SpawnActor<AWeapon>(weaponClass);
			weapon->becomeEquipped(this); // <-- and this function will call MainCharacter::equip() ...
		}
	}
	
	// ========== make char NOT dead (just in case)
	movementState = MovementStateEnum::Normal;
	GetMesh()->bPauseAnims = false;
	GetMesh()->bNoSkeletonUpdate = false;
}

// ======================================== Debug stuff
void AMainCharacter::showPickupLocations()
{
	for (FVector &location : pickupLocations)
	{
		// Example of draw sphere for debug purpose
		UKismetSystemLibrary::DrawDebugSphere(this, location, 25.f, 24,
		                                      FLinearColor::Green, 10.f, .5f);
	}
}

// ======================================== Common
void AMainCharacter::Jump()
{
	if (movementState != MovementStateEnum::Dead)
	{
		Super::Jump();
	}
}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (movementState != MovementStateEnum::Dead)
	{
		FVector speed = GetVelocity();
		currentMovementSpeed = speed.Size2D(); // Currently used of animations states and for stamina update
		if (currentMovementSpeed == 0)
			setSprintOff();

		updateStaminaState(DeltaTime);
		turnToEnemyInterp(DeltaTime);
	}

	// Update enemy location
	if (attackTarget)
	{
		attackTargetLocation = attackTarget->GetTargetLocation();
		if (mainPlayerController)
			mainPlayerController->enemyLocation = attackTargetLocation; // So why do we need attackTargetLocation?
	}
}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);
	// Note: check() - stops execution if expression is FALSE
	// https://docs.unrealengine.com/en-US/Programming/Assertions/index.html

	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &AMainCharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &AMainCharacter::setShiftPressed);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &AMainCharacter::setShiftReleased);

	PlayerInputComponent->BindAction(TEXT("LMB"), IE_Pressed, this, &AMainCharacter::setLMBPressed);
	PlayerInputComponent->BindAction(TEXT("LMB"), IE_Released, this, &AMainCharacter::setLMBReleased);

	PlayerInputComponent->BindAction(TEXT("Esc"), IE_Pressed, this, &AMainCharacter::escPress);

	// ==================================================
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AMainCharacter::moveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AMainCharacter::moveRight);

	// Camera movement BY MOUSE
	PlayerInputComponent->BindAxis(TEXT("CameraYaw"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis(TEXT("CameraPitch"), this, &APawn::AddControllerPitchInput);

	// Camera movement BY KEYBOARD (which call same functions as mouse move call: AddController...Input())
	PlayerInputComponent->BindAxis(TEXT("CameraTurn"), this, &AMainCharacter::cameraTurn);
	PlayerInputComponent->BindAxis(TEXT("CameraLookUp"), this, &AMainCharacter::cameraLookUp);
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
// ======================================== Common
// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	// ======================================== Common
	mainPlayerController = Cast<AMainPlayerController>(GetController());

	//TODO: load game state (weapon, stats, etc) when started new level <----------
	// Note: can't done it with current v. lessons approach while 1 profile is used
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
// ======================================== Movement + Camera movement
void AMainCharacter::moveForward(float inputValue)
{
	if (Controller != nullptr && inputValue != 0.f && !attacking && movementState != MovementStateEnum::Dead)
	{
		if (shiftPressed)
			setSprintOn();

		// Find out which way is forward
		const FRotator rotation = Controller->GetControlRotation();
		const FRotator yawRotation(0.f, rotation.Yaw, 0.f); // Z-axis. Turn head left/right

		// Direction which camera is looking
		const FVector direction = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(direction, inputValue);
	}
}

void AMainCharacter::moveRight(float inputValue)
{
	if (Controller != nullptr && inputValue != 0.f && !attacking && movementState != MovementStateEnum::Dead)
	{
		if (shiftPressed)
			setSprintOn();

		// Find out which way is forward
		const FRotator rotation = Controller->GetControlRotation();
		const FRotator yawRotation(0.f, rotation.Yaw, 0.f); // Z-axis. Turn head left/right

		// Direction which camera is looking, BUT, Y-axis (so it's probably perpendicular to camera direction?)
		const FVector direction = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(direction, inputValue);
	}
}

void AMainCharacter::cameraTurn(float inputValue)
{
	if (movementState != MovementStateEnum::Dead)
	{
		// Below formula is: inputValue * degrees per second
		AddControllerYawInput(inputValue * cameraTurnRate * GetWorld()->GetDeltaSeconds());
	}
}

void AMainCharacter::cameraLookUp(float inputValue)
{
	if (movementState != MovementStateEnum::Dead)
	{
		// Below formula is: inputValue * degrees per second
		AddControllerPitchInput(inputValue * cameraLookUpRate * GetWorld()->GetDeltaSeconds());
	}
}

// ======================================== Run/Sprint + Stamina (done differently than in v. lesson)
// My version of code isn't good (because no state machine, but at least less duplicate code)
void AMainCharacter::setShiftPressed()
{
	shiftPressed = true;
	setSprintOn();
}

void AMainCharacter::setShiftReleased()
{
	shiftPressed = false;
	setSprintOff();
}

void AMainCharacter::setSprintOn()
{
	if (staminaState != StaminaStateEnum::Exhausted && staminaState != StaminaStateEnum::Exhausted_Recovering &&
		currentMovementSpeed > 0)
	{
		movementState = MovementStateEnum::Sprint;
		GetCharacterMovement()->MaxWalkSpeed = sprintSpeed;
	}
}

void AMainCharacter::setSprintOff()
{
	if (movementState != MovementStateEnum::Dead)
	{
		movementState = MovementStateEnum::Normal;
		GetCharacterMovement()->MaxWalkSpeed = runSpeed;
	}
}

void AMainCharacter::updateStaminaState(float DeltaTime)
{
	float deltaStamina = staminaDrainRate * DeltaTime;

	switch (staminaState)
	{
	case StaminaStateEnum::Normal:
		updateStamina(deltaStamina);
		if (stamina <= minSprintStamina)
			setStaminaState(StaminaStateEnum::Below_Minimum);
		break;

	case StaminaStateEnum::Below_Minimum:
		// Change stamina color to yellow

		updateStamina(deltaStamina);
		if (stamina > minSprintStamina)
			setStaminaState(StaminaStateEnum::Normal);
		else if (stamina == 0)
		{
			setSprintOff();
			setStaminaState(StaminaStateEnum::Exhausted);
		}
		break;

	case StaminaStateEnum::Exhausted:
		if (!shiftPressed)
			setStaminaState(StaminaStateEnum::Exhausted_Recovering);
		break;

	case StaminaStateEnum::Exhausted_Recovering:
		// Change stamina color to orange

		updateStamina(deltaStamina);
		if (stamina > minSprintStamina)
		{
			setStaminaState(StaminaStateEnum::Normal);
			if (shiftPressed)
				setSprintOn();
		}
		break;
	}
}

void AMainCharacter::updateStamina(float deltaStamina)
{
	if (movementState == MovementStateEnum::Sprint && stamina > 0)
	{
		stamina -= deltaStamina;
		stamina = (stamina < 0) ? 0 : stamina;
	}
	else if (stamina < maxStamina)
	{
		stamina += deltaStamina;
		stamina = (stamina > maxStamina) ? maxStamina : stamina;
	}
}

// ======================================== Turn to enemy interpolation (Lock on target)
void AMainCharacter::setInterpToEnemy(bool value)
{
	interpToEnemy = value;
}

void AMainCharacter::turnToEnemyInterp(float DeltaTime)
{
	// Interpolating to enemy
	if (interpToEnemy && attackTarget)
	{
		// We have "rotation to target direction"
		FRotator lookAtYaw = getLookAtRotationYaw(attackTarget->GetTargetLocation());

		// Now we will interpolate to that direction (instead of quick change direction of player)
		FRotator interpRotation = FMath::RInterpTo(GetActorRotation(), lookAtYaw, DeltaTime, interpToEnemySpeed);
		SetActorRotation(interpRotation);
	}
}

FRotator AMainCharacter::getLookAtRotationYaw(FVector targetLocation)
{
	FRotator lookAtRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), targetLocation);
	FRotator lookAtRotationYaw(0.f, lookAtRotation.Yaw, 0.f);

	return lookAtRotationYaw;
}

void AMainCharacter::updateAttackTarget()
{
	TArray<AActor*> overlappingActors;
	TSubclassOf<AEnemy> filter;
	// Note: this doesn't work always, or collisions need to be configured. I checked.
	GetOverlappingActors(overlappingActors, filter);
	if (overlappingActors.Num() == 0)
	{
		attackTarget = nullptr;
		return;
	}

	// ===== Find closest enemy =====
	AEnemy *closestEnemy = Cast<AEnemy>(overlappingActors[0]);
	float minDistance = (closestEnemy->GetActorLocation() - GetActorLocation()).Size();

	AEnemy *newClosestEnemy;
	float newMinDistance;
	for (int i = 1; i < overlappingActors.Num(); i++)
	{
		newClosestEnemy = Cast<AEnemy>(overlappingActors[i]);
		newMinDistance = (newClosestEnemy->GetActorLocation() - GetActorLocation()).Size();

		if (newMinDistance < minDistance)
		{
			closestEnemy = newClosestEnemy;
			minDistance = newMinDistance;
		}
	}

	// ===== Update attack target =====
	if (mainPlayerController)
	{
		mainPlayerController->displayEnemyHealthBar();
	}
	setAttackTarget(closestEnemy);
}

// ======================================== Common
void AMainCharacter::setLMBPressed()
{
	lmbPressed = true;

	if (movementState == MovementStateEnum::Dead)
		return;

	if (overlappingItem)
	{
		// ======================================== Weapon/Item (weapon equip)
		AWeapon *weapon = Cast<AWeapon>(overlappingItem);
		if (weapon)
		{
			weapon->becomeEquipped(this);
		}
	}
	else if (equipedWeapon && !attacking)
	{
		// ======================================== Attack
		attack();
	}
}

void AMainCharacter::setLMBReleased()
{
	lmbPressed = false;
}

void AMainCharacter::escPress()
{
	if (mainPlayerController)
		mainPlayerController->togglePauseMenu();
}
