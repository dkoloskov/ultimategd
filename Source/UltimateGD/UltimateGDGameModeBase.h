// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UltimateGDGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ULTIMATEGD_API AUltimateGDGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
