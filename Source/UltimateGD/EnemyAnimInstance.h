// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "Enemy.h"
#include "EnemyAnimInstance.generated.h"


/**
 * 
 */
UCLASS()
class ULTIMATEGD_API UEnemyAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	virtual void NativeInitializeAnimation() override;

	UFUNCTION(BlueprintCallable, Category = AnimationProperties)
	void updateAnimationProperties();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Movement)
	float movementSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Movement)
	APawn *pawn;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Movement)
	AEnemy *enemy;
};
