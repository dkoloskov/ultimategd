// Fill out your copyright notice in the Description page of Project Settings.
#include "SpawnVolume.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "Enemy.h"
#include "AIController.h"


// Sets default values
ASpawnVolume::ASpawnVolume()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	spawningBox = CreateDefaultSubobject<UBoxComponent>(TEXT("spawningBox"));
	RootComponent = spawningBox;
}

//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
FVector ASpawnVolume::getSpawnPoint()
{
	// extent - ������, ����
	FVector extent = spawningBox->GetScaledBoxExtent();
	FVector origin = spawningBox->GetComponentLocation();

	FVector point = UKismetMathLibrary::RandomPointInBoundingBox(origin, extent);
	return point;
}

TSubclassOf<AActor> ASpawnVolume::getSpawnActor()
{
	if (spawnArray.Num() > 0)
	{
		int32 selection = FMath::RandRange(0, spawnArray.Num() - 1);
		return spawnArray[selection];
	}

	return nullptr;
}

// default implementation of "BlueprintNativeEvent" function
void ASpawnVolume::spawnActor_Implementation(TSubclassOf<AActor> actorClass, const FVector &location)
{
	// Note:
	// with "TSubclassOf<ACreature>" - we have type check during compilation, so wrong type can't be passed
	// we can use "UClass*" instead - in that case type check will be performed during runtime, which is good way to have bugs

	if (actorClass)
	{
		UWorld *world = GetWorld();

		if (world)
		{
			AActor *actor = world->SpawnActor<AActor>(actorClass, location, FRotator(0.f));

			AEnemy *enemy = Cast<AEnemy>(actor);
			if (enemy)
			{
				enemy->SpawnDefaultController(); // This isn't AI controller

				// We need to set aiController manually, because we used SpawnActor()
				// which isn't adding default controller, because we can spawn for instance: Item (and item don't have controller :) )
				AAIController *aiControlelr = Cast<AAIController>(enemy->GetController());
				if (aiControlelr)
				{
					enemy->aiController = aiControlelr;
				}
			}
		}
	}
}

// ======================================== Common
// Called every frame
void ASpawnVolume::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
// ======================================== Common
// Called when the game starts or when spawned
void ASpawnVolume::BeginPlay()
{
	Super::BeginPlay();

	if (actor_1 && actor_2 && actor_3 && actor_4)
	{
		spawnArray.Add(actor_1);
		spawnArray.Add(actor_2);
		spawnArray.Add(actor_3);
		spawnArray.Add(actor_4);
	}
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
