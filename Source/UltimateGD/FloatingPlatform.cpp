// Fill out your copyright notice in the Description page of Project Settings.
#include "FloatingPlatform.h"
#include "Math/UnrealMathUtility.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// Sets default values
AFloatingPlatform::AFloatingPlatform()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// ======================== MY ADDED ITEMS (unreal default's above) ========================
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("mesh"));
	RootComponent = mesh;

	//
	endPoint = FVector(0.f);
	interpSpeed = 4.f;

	flyState = false;
	flyDelay = 1.f;
}

// Called every frame
void AFloatingPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (flyState)
	{
		// ======================================== FLY
		FVector currentLocation = GetActorLocation();
		FVector interp = FMath::VInterpTo(currentLocation, endPoint, DeltaTime, interpSpeed);
		SetActorLocation(interp);
		// Note: after see how it works in game... I can say that - THIS IS NOT GOOD APPROACH to move platform 
		// Better use Timeline in BP next time. Currently done like in video lesson.

		// ======================================== STOP FLY (+ prepare for fly back)
		float distanceTraveled = (GetActorLocation() - startPoint).Size();
		if (flyDistance - distanceTraveled <= 1.f)
		{
			toggleFlyState();
			swapStartAndEndPoints(startPoint, endPoint);
			setFlyDelayTimer();
		}
	}
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
// Called when the game starts or when spawned
void AFloatingPlatform::BeginPlay()
{
	Super::BeginPlay();

	startPoint = GetActorLocation();
	endPoint += startPoint; // Because endPoint location is relative to startPoint
	// TODO: when this object scaled - endPoint position is wrong (update above with scale taken in mind)

	flyDistance = (endPoint - startPoint).Size();
	setFlyDelayTimer();
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
void AFloatingPlatform::setFlyDelayTimer()
{
	// Global timer from "actors world"
	GetWorldTimerManager().SetTimer(flyDelayHandle, this, &AFloatingPlatform::toggleFlyState, flyDelay);
}

void AFloatingPlatform::toggleFlyState()
{
	flyState = !flyState;
}

void AFloatingPlatform::swapStartAndEndPoints(FVector &v1, FVector &v2)
{
	FVector temp = v1;
	v1 = v2;
	v2 = temp;
}
