// Fill out your copyright notice in the Description page of Project Settings.
#include "Creature.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/InputComponent.h"


// Sets default values
ACreature::ACreature()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// ======================================== Create Static Mesh Component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("rootComponent"));
	meshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("meshComponent"));
	meshComp->SetupAttachment(GetRootComponent());

	// ======================================== Create and setup Camera
	cameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("cameraComponent"));
	cameraComp->SetupAttachment(GetRootComponent());
	cameraComp->SetRelativeLocation(FVector(-300.f, 0.f, 300.f));
	cameraComp->SetRelativeRotation(FRotator(-45.f, 0.f, 0.f));

	// ======================================== Pawn Movement
	// Player0 - it's the ONLY player in single game (first in multiplayer)
	// AutoPossessPlayer = EAutoReceiveInput::Player0;

	currentVelocity = FVector(0.f);
	maxSpeed = 100.f;
}

//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// ======================================== Pawn Movement
// Called every frame
void ACreature::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector newLocation = GetActorLocation() + (currentVelocity * DeltaTime);
	SetActorLocation(newLocation);
	//UE_LOG(LogTemp, Warning, TEXT("x:%f, y:%f, z:%f"), newLocation.X, newLocation.Y, newLocation.Z);
}

// Called to bind functionality to input
void ACreature::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ACreature::moveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ACreature::moveRight);
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
// Called when the game starts or when spawned
void ACreature::BeginPlay()
{
	Super::BeginPlay();
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
// ======================================== Pawn Movement
void ACreature::moveForward(float value)
{
	// FMath::Clamp() - CUT the value if it's lower that Min or higher that Max
	currentVelocity.X = FMath::Clamp(value, -1.f, 1.f) * maxSpeed;
}

void ACreature::moveRight(float value)
{
	currentVelocity.Y = FMath::Clamp(value, -1.f, 1.f) * maxSpeed;
}
