// Fill out your copyright notice in the Description page of Project Settings.
#include "EnemyAnimInstance.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
void UEnemyAnimInstance::NativeInitializeAnimation()
{
	if (pawn == nullptr)
	{
		pawn = TryGetPawnOwner();
		if (pawn)
		{
			enemy = Cast<AEnemy>(pawn);
		}
	}
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
void UEnemyAnimInstance::updateAnimationProperties()
{
	if (pawn == nullptr)
	{
		return;
	}

	FVector speed = pawn->GetVelocity();
	movementSpeed = speed.Size2D(); // We need speed only in X|Y directions

	//UE_LOG(LogTemp, Error, TEXT("EnemyAnim::movementSpeed %f"), movementSpeed);
}
