// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "TimerManager.h"
#include "FloorSwitch.generated.h"


UCLASS()
class ULTIMATEGD_API AFloorSwitch : public AActor
{
	GENERATED_BODY()

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// ======================== MY ADDED ITEMS (unreal default's above) ========================
public:
	// Sets default values for this actor's properties
	AFloorSwitch();

	/**
	 * Volume - for functionality to be triggered
	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Floor Switch")
	UBoxComponent *triggerBox;

	/**
	 * Switch for the character to step on
	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Floor Switch")
	UStaticMeshComponent *floorSwitch;

	/**
	 * Door to move, when the floor switch is stepped on
	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Floor Switch")
	UStaticMeshComponent *door;

	UPROPERTY(BlueprintReadWrite, Category = "Floor Switch")
	FVector initialDoorLocation;

	UPROPERTY(BlueprintReadWrite, Category = "Floor Switch")
	FVector initialSwitchLocation;

	// ==================================================
	UPROPERTY(EditAnywhere, Category = "Floor Switch")
	float closeDoorDelay;

	// ==================================================
	UFUNCTION() // <--- this is MUST, for overlap functions
	void onOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor, UPrimitiveComponent *OtherComp,
	                    int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION() // <--- this is MUST, for overlap functions
	void onOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor, UPrimitiveComponent *OtherComp,
	                  int32 OtherBodyIndex);

	// +++++++++++++++++++++++++ EVENTS +++++++++++++++++++++++++
	UFUNCTION(BlueprintImplementableEvent, Category = "Floor Switch")
	void raiseDoor();
	// Note: BlueprintImplementableEvent - no need to provide implementation in c++

	UFUNCTION(BlueprintImplementableEvent, Category = "Floor Switch")
	void lowerDoor();

	UFUNCTION(BlueprintImplementableEvent, Category = "Floor Switch")
	void raiseFloorSwitch();

	UFUNCTION(BlueprintImplementableEvent, Category = "Floor Switch")
	void lowerFloorSwitch();
	// ------------------------- EVENTS -------------------------

	//
	UFUNCTION(BlueprintCallable, Category = "Floor Switch")
	void updateDoorLocation(float z);

	UFUNCTION(BlueprintCallable, Category = "Floor Switch")
	void updateFloorSwitchLocation(float z);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	FTimerHandle closeDoorDelayHandle; // NOT Handler, Delegate for timer

	bool characterOnFloorSwitch;

	void closeDoor();
};
