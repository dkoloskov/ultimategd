// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Animation/AnimMontage.h"
#include "Particles/ParticleSystem.h"
#include "Sound/SoundCue.h"
#include "MainCharacter.generated.h"


// ======================================== Movement state ENUM
// Sh*tty example of state machine (done according to v. lesson)
UENUM(BlueprintType)
enum class MovementStateEnum : uint8
{
	Normal UMETA(DisplayName = "Normal"), // <--- UMETA(DisplayName = ...) - MUST BE added if UENUM is used
	Sprint UMETA(DisplayName = "Sprint"), // <---
	Dead UMETA(DisplayName = "Dead"),

	Max UMETA(DisplayName = "DefaultMax") // This is just "number of values" in Enum. Good UE practice to have it.
};
// BlueprintType - Exposes this class as a type that can be used for variables in Blueprints.

// ======================================== Stamina state ENUM
UENUM(BlueprintType)
enum class StaminaStateEnum : uint8
{
	Normal UMETA(DisplayName = "Normal"), // <--- UMETA(DisplayName = ...) - MUST BE added if UENUM is used
	Below_Minimum UMETA(DisplayName = "Below_Minimum"), // <---
	Exhausted UMETA(DisplayName = "Exhausted"), // <---
	Exhausted_Recovering UMETA(DisplayName = "Exhausted_Recovering"), // <---

	Max UMETA(DisplayName = "DefaultMax") // This is just "number of values" in Enum. Just good practice to have it.
};
// IMPORTANT NOTE: enums above can be moved to "UserDefinedEnum" classes (90% sure)
// Currently done according to v. lessons

// TODO: In terms of refactoring functionality below can be moved to these classes: Movement, Camera, Stats
// Note: "to do" above related for next projects, current project will stay as-is
UCLASS()
class ULTIMATEGD_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

	// ======================== MY ADDED ITEMS (unreal default's above) ========================
public:
	AMainCharacter();

	// ======================================== Create and setup Camera
	/**
	 * Positioning the camera behind the player
	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent *cameraSpringComp;
	// Note: meta = (AllowPrivateAccess = "true") - makes this accessible only inside Blueprint

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent *followCameraComp;

	FORCEINLINE USpringArmComponent* getCameraSpringComonent() const { return cameraSpringComp; }
	FORCEINLINE UCameraComponent* getFollowCameraComonent() const { return followCameraComp; }

	// ======================================== Movement + Camera movement
	/**
	 * Camera turn rates for input
	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float cameraTurnRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float cameraLookUpRate;

	// ======================================== Run/Sprint + Stamina
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float runSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float sprintSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float currentMovementSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float staminaDrainRate; // Also used as "recovery rate"
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float minSprintStamina;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
	MovementStateEnum movementState;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
	StaminaStateEnum staminaState;

	// ======================================== Player Stats
	/**
	 *
	 * <<<<< Player Stats >>>>>
	 * 
	 */

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Stats")
	float maxHealth;
	// EditDefaultsOnly - ����� ������������� ������ Editor, �� �� Instance
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Stats")
	float health;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player Stats")
	float maxStamina;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Stats")
	float stamina;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Stats")
	int32 coins;

	//
	UFUNCTION(BlueprintCallable)
	void increaseHealth(float value);
	void decreaseHealth(float value);

	UFUNCTION(BlueprintCallable)
	void increaseCoinsAmount(int32 value);

	// ======================================== Weapon/Item
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Items")
	class AWeapon *equipedWeapon; // No import, because of circular dependency error...

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Items")
	class AItem *overlappingItem;

	void setEquippedWeapon(AWeapon *weapon);
	FORCEINLINE AWeapon* getEquippedWeapon() { return equipedWeapon; }

	FORCEINLINE void setOverlappingItem(AItem *item) { overlappingItem = item; }

	// ======================================== Attack
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Animations")
	bool attacking;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	UAnimMontage *combatMontage;

	/**
	 * key - Attack Name
	 * value - Attack Speed
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	TMap<FString, float> attackAnimationSpeed;
	// This way fill be more safe, that iterating through TMap
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animations")
	TArray<FString> attackNames;

	void attack();

	// This methods will be called in ...Animation_BP, on "EndAttacking" Notify (Event)
	UFUNCTION(BlueprintCallable)
	void attackEnd();

	// ======================================== Take damage
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Combat")
	UParticleSystem *hitParticles; // will be shown, when enemy is damaged

	virtual float TakeDamage(float Damage, FDamageEvent const &DamageEvent, AController *EventInstigator,
	                         AActor *DamageCauser) override;

	// ======================================== Death + Disappear
	void die();

	UFUNCTION(BlueprintCallable)
	void deathEnd(); // Will be called from SpiderAnim_BP on notify from SpiderCombatMontage

	//
	/*FTimerHandle disapearDelayHandle; // v. lessons name: DeathTimer

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
	float disappearDelay; // v. lessons name: DeathDelay

	void disappear();*/

	// ======================================== Turn to enemy interpolation (Lock on target)
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat")
	class AEnemy *attackTarget; // combatTarget in v. lessons

	FORCEINLINE void setAttackTarget(AEnemy *target) { attackTarget = target; }

	//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
	TSubclassOf<AEnemy> enemyFilter;

	void updateAttackTarget(); // We can do it on attackEnd()... But I will do it v. lesson way

	// ======================================== Enemy health bar
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Combat")
	FVector attackTargetLocation;

	// ======================================== Sounds
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
	USoundCue *hitSound; // play when got hit by player

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
	float hitSoundVolume;

	// ======================================== Other Global functionality (which shouldn't be here)
	void switchLevel(FName levelName);

	//
	UFUNCTION(BlueprintCallable)
	void saveGame();

	UFUNCTION(BlueprintCallable)
	void loadGame(bool setPosition);
	
	// ======================================== Item Storage (storage for save game...)
	// Note: I think this is very stupid idea, to have BP with ALL weapons in this class. And we need this only to save weapon on "save game".
	UPROPERTY(EditDefaultsOnly, Category = "SaveData")
	TSubclassOf<class AItemsStorage> weaponStorage;
	
	// ======================================== Debug stuff
	TArray<FVector> pickupLocations;

	UFUNCTION(BlueprintCallable)
	void showPickupLocations();

	// ======================================== Common
	bool lmbPressed; // Left Mouse Button

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Controller")
	class AMainPlayerController *mainPlayerController;

	virtual void Jump() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

protected:
	// ======================================== Common
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// ======================================== Movement + Camera movement
	// Move forward/backward
	void moveForward(float inputValue);
	// Move right/left
	void moveRight(float inputValue);

	/**
	 * Called via input to turn at a given rate
	 * @param inputValue - normalized rate. 1.0 means 100% of desired turn rate
	 */
	void cameraTurn(float inputValue);
	/**
	 * Called via input to look up/down at a given rate
	 * @param inputValue - normalized rate. 1.0 means 100% of desired look up/down rate
	 */
	void cameraLookUp(float inputValue);

	// ======================================== Run/Sprint + Stamina
	bool shiftPressed;

	void setShiftPressed();
	void setShiftReleased();

	// In normal project will make normal state machine
	void setSprintOn();
	void setSprintOff();

	void updateStaminaState(float DeltaTime); // no such method in v. lessons
	FORCEINLINE void setStaminaState(StaminaStateEnum value) { staminaState = value; }

	void updateStamina(float deltaStamina);

	// ======================================== Turn to enemy interpolation (Lock on target)
	float interpToEnemySpeed;
	bool interpToEnemy;
	void setInterpToEnemy(bool value); // I know that this is useless function (setter for private variable)

	void turnToEnemyInterp(float DeltaTime); // no such method in v. lessons
	FRotator getLookAtRotationYaw(FVector targetLocation); // rotation to target direction

	// ======================================== Common
	void setLMBPressed();
	void setLMBReleased();

	void escPress(); // Escape press
};
