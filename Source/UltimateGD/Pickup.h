// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "Item.h"
#include "Pickup.generated.h"


/**
 * 
 */
UCLASS()
class ULTIMATEGD_API APickup : public AItem
{
	GENERATED_BODY()

public:
	APickup();

	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Coins")
	int32 coinCount;*/ // moved to BP

	// ==================================================
	//UFUNCTION() // <--- no need for this here, since it's already in parent (uncomment will give ERROR)
	virtual void onOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                            UPrimitiveComponent *OtherComp,
	                            int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult) override;

	//UFUNCTION() // <--- no need for this here, since it's already in parent (uncomment will give ERROR)
	virtual void onOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                          UPrimitiveComponent *OtherComp,
	                          int32 OtherBodyIndex) override;

	// ==================================================
	UFUNCTION(BlueprintImplementableEvent, Category = "Pickup")
	void onPickup(class AMainCharacter *target);
};
