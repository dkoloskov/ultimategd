// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/SphereComponent.h"
#include "AIController.h"
#include "MainCharacter.h"
#include "Particles/ParticleSystem.h"
#include "Sound/SoundCue.h"
#include "Components/BoxComponent.h"
#include "Enemy.generated.h"


UENUM(BlueprintType)
enum class EnemyMovementState : uint8
{
	Idle UMETA(DisplayName = "Idle"), // <--- UMETA(DisplayName = ...) - MUST BE added if UENUM is used
	MoveToTarget UMETA(DisplayName = "MoveToTarget"), // <---
	Attacking UMETA(DisplayName = "Attacking"), // <---
	Dead UMETA(DisplayName = "Dead"), // <---

	Max UMETA(DisplayName = "DefaultMax") // This is just "number of values" in Enum. Good UE practice to have it.
};


UCLASS()
class ULTIMATEGD_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	// ======================================== Movement
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
	EnemyMovementState movementState;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI")
	AAIController *aiController;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Movement")
	float movementAcceptanceRadius;

	// ======================================== Collision / Combat
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI")
	USphereComponent *agroSphere; // In this sphere - enemy will chase player

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI")
	USphereComponent *combatSphere; // In this sphere - enemy will attack player

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AI")
	bool overlappingCombatSphere;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI")
	AMainCharacter *combatTarget; // variable to follow (MoveTo()) "player"

	// Agro radius
	UFUNCTION() // <--- this is MUST, for overlap functions
	virtual void onAgroOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                                UPrimitiveComponent *OtherComp,
	                                int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION() // <--- this is MUST, for overlap functions
	virtual void onAgroOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                              UPrimitiveComponent *OtherComp,
	                              int32 OtherBodyIndex);

	// Combat radius
	UFUNCTION() // <--- this is MUST, for overlap functions
	virtual void onCombatOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                                  UPrimitiveComponent *OtherComp,
	                                  int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION() // <--- this is MUST, for overlap functions
	virtual void onCombatOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                                UPrimitiveComponent *OtherComp,
	                                int32 OtherBodyIndex);

	// ======================================== Enemy Stats
	/**
	 *
	 * <<<<< Enemy Stats >>>>>
	 *
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enemy Stats")
	float maxHealth;
	// EditDefaultsOnly - ����� ������������� ������ Editor, �� �� Instance
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Stats")
	float health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Stats")
	float damage;

	//
	void increaseHealth(float value);
	void decreaseHealth(float value);

	bool alive();
	
	// ======================================== Attack
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Combat")
	UBoxComponent *attackCollision; // v. lesson name: combatCollision

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat")
	bool attacking;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat")
	UAnimMontage *combatMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat")
	float attackAnimationSpeed;

	void attack();

	// This methods will be called in SpiderAnimation_BP, on "EndAttacking" Notify (Event)
	UFUNCTION(BlueprintCallable)
	void attackEnd();

	// This functions called on from SpiderAnim_BP on "ActivateCollision","DeactivateCollision" ...
	// ... notify from CombatMontage
	UFUNCTION(BlueprintCallable)
	void activateCollision();
	UFUNCTION(BlueprintCallable)
	void deactivateCollision();

	// This function called on from SpiderAnim_BP, on "PlaySwingSound" from CombatMontage
	UFUNCTION(BlueprintCallable)
	void playSwingSound();

	UFUNCTION() // <--- this is MUST, for overlap functions
	void onAttackOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                          UPrimitiveComponent *OtherComp,
	                          int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION() // <--- this is MUST, for overlap functions
	void onAttackOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                        UPrimitiveComponent *OtherComp,
	                        int32 OtherBodyIndex);
	// Note: v. lesson method name: onCombatOverlapBegin()/...End()

	// ======================================== Attack delay
	FTimerHandle attackDelayHandle; // // NOT Handler, Delegate for timer    v. lesson name: attackTimer

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
	float attackMinDelay;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
	float attackMaxDelay;

	// ======================================== Damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
	TSubclassOf<UDamageType> damageTypeClass; // some UE special way for dealing damage
	// with "TSubclassOf<UUserWidget>" - we have type check during compilation, so wrong type can't be passed
	// we can use "UClass*" instead - in that case type check will be performed during runtime, which is good way to have bugs

	// ======================================== Take damage
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI")
	UParticleSystem *hitParticles; // will be shown, when enemy is damaged

	virtual float TakeDamage(float Damage, FDamageEvent const &DamageEvent, AController *EventInstigator,
	                         AActor *DamageCauser) override;

	// ======================================== Death + Disappear
	void die(AActor *causer);

	UFUNCTION(BlueprintCallable)
	void deathEnd(); // Will be called from SpiderAnim_BP on notify from SpiderCombatMontage

	//
	FTimerHandle disapearDelayHandle; // v. lessons name: DeathTimer
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
	float disappearDelay; // v. lessons name: DeathDelay

	void disappear();

	// ======================================== Sounds
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	USoundCue *hitSound; // play when got hit by player

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	float hitSoundVolume;

	//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	USoundCue *swingSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	float swingSoundVolume;

	// ======================================== Common
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// ======================================== Movement
	FORCEINLINE void setEnemyMovementState(EnemyMovementState state) { movementState = state; }

	void moveToTarget(AMainCharacter *target);

	void tryFollowTarget(); // no such method in v. lessons
};
