// Fill out your copyright notice in the Description page of Project Settings.
#include "ColliderMovementComponent.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// ���������� ����� �������...
void UColliderMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                               FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// Check to make sure everything is still valid and that we allowed to move
	if (!PawnOwner || !UpdatedComponent || ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	// Get and clear the vector from Collider
	FVector desiredMovementThisFrame = ConsumeInputVector().GetClampedToMaxSize(1.f) * DeltaTime * 150.f;
	// Magic numbers...
	if (!desiredMovementThisFrame.IsNearlyZero())
	{
		FHitResult hit;
		SafeMoveUpdatedComponent(desiredMovementThisFrame, UpdatedComponent->GetComponentRotation(), true, hit);

		// If we bump into something, slid along the side of it
		if (hit.IsValidBlockingHit())
		{
			// "1.f - hit.Time" - done, as written in function description
			SlideAlongSurface(desiredMovementThisFrame, 1.f - hit.Time, hit.Normal, hit);
		}
	}
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
