// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "Item.h"
#include "Components/SkeletalMeshComponent.h"
#include "MainCharacter.h"
#include "Sound/SoundCue.h"
#include "Components/BoxComponent.h"
#include "Weapon.generated.h"


UENUM(BlueprintType)
enum class WeaponStateEnum : uint8
{
	Pickup UMETA(DisplayName = "Pickup"),
	Equipped UMETA(DisplayName = "Equipped"),

	Max UMETA(DisplayName = "Equipped")
};

/**
 * 
 */
UCLASS()
class ULTIMATEGD_API AWeapon : public AItem
{
	GENERATED_BODY()

public:
	AWeapon();

	UPROPERTY(EditDefaultsOnly, Category = "SaveData")
	FString name; // Variable required only for "save game" data

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SkeletalMesh")
	USkeletalMeshComponent *skeletalMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Particles")
	bool showParticles; // After pickup weapon, in player hand show/hide particles on weapon

	// ======================================== Equip
	// Attach skeletalMesh to mainCharacter RightHandSocket ("equip(...)" original name in v. lessons)
	void becomeEquipped(AMainCharacter *mainCharacter);
	// Note: item SHOULD NOT have logic how it must be equipped...

	//
	//UFUNCTION() // <--- no need for this here, since it's already in parent (uncomment will give ERROR)
	virtual void onOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                            UPrimitiveComponent *OtherComp,
	                            int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult) override;

	//UFUNCTION() // <--- no need for this here, since it's already in parent (uncomment will give ERROR)
	virtual void onOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                          UPrimitiveComponent *OtherComp,
	                          int32 OtherBodyIndex) override;

	// ======================================== Weapon state
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item")
	WeaponStateEnum weaponState;

	FORCEINLINE void setWeaponState(WeaponStateEnum state) { weaponState = state; }
	FORCEINLINE WeaponStateEnum getWeaponState() { return weaponState; }

	// ======================================== Attack
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item | Combat")
	UBoxComponent *attackCollision;

	// This functions called on from MainCharacterAnim_BP on "ActivateCollision","DeactivateCollision" ...
	// ... notify from CombatMontage
	UFUNCTION(BlueprintCallable)
	void activateCollision();
	UFUNCTION(BlueprintCallable)
	void deactivateCollision();

	// This function also called on from MainCharacterAnim_BP, on "PlaySwingSound" from CombatMontage
	UFUNCTION(BlueprintCallable)
	void playSwingSound(); // in v. lessons this method in MainCharacter

	UFUNCTION() // <--- this is MUST, for overlap functions
	void onAttackOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                          UPrimitiveComponent *OtherComp,
	                          int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION() // <--- this is MUST, for overlap functions
	void onAttackOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                        UPrimitiveComponent *OtherComp,
	                        int32 OtherBodyIndex);
	// Note: v. lesson method name: onCombatOverlapBegin()/...End()

	// ======================================== Damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Combat")
	TSubclassOf<UDamageType> damageTypeClass; // some UE special way for dealing damage
	// with "TSubclassOf<UUserWidget>" - we have type check during compilation, so wrong type can't be passed
	// we can use "UClass*" instead - in that case type check will be performed during runtime, which is good way to have bugs

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item | Combat")
	AController *weaponInstigator; // stores MainCharacter Controller (required fro "ApplyDamage" function)
	// instigator - "подстрекатель"

	FORCEINLINE void setWeaponInstigator(AController *value) { weaponInstigator = value; }

	// ======================================== Weapon Stats
	/**
	 *
	 * <<<<< Weapon Stats >>>>>
	 *
	 */
	UPROPERTY
	(EditAnywhere, BlueprintReadWrite, Category = "Item | Stats")
	float damage;

	// ======================================== Sounds
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Sounds")
	USoundCue *equipSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Sounds")
	float equipSoundVolume;

	//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Sounds")
	USoundCue *swingSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Sounds")
	float swingSoundVolume;

protected:
	// ======================================== Common
	virtual void BeginPlay() override;
};
