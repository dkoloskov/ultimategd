// Fill out your copyright notice in the Description page of Project Settings.
#include "FloorSwitch.h"


// Called every frame
void AFloorSwitch::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// ======================== MY ADDED ITEMS (unreal default's above) ========================
//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// Sets default values
AFloorSwitch::AFloorSwitch()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// ==================================================
	triggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("triggerBox"));
	RootComponent = triggerBox; // � ����� ����� �����, ��� �� ������� triggerBox �����, ��� ���������� �� ��� � ����...

	// Collision only for: overlap, raycast, etc. No physics collision
	triggerBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	triggerBox->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	// Set collision response only for 1 specific chanel
	triggerBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	triggerBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	// Note: I think it's better to setup this specific stuff in Blueprint

	// Set size befor scale
	triggerBox->SetBoxExtent(FVector(62.f, 62.f, 32.f));
	// Note: again, this hardcode better to do in Blueprint

	// ==================================================
	floorSwitch = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("floorSwitch"));
	floorSwitch->SetupAttachment(GetRootComponent());

	door = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("door"));
	door->SetupAttachment(GetRootComponent());

	// ==================================================
	closeDoorDelay = 2.f;
	characterOnFloorSwitch = false;
}

void AFloorSwitch::onOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                  UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                  const FHitResult &SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Overlap Begin"));
	characterOnFloorSwitch = true;

	raiseDoor();
	lowerFloorSwitch();
}

void AFloorSwitch::onOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("Overlap End"));
	characterOnFloorSwitch = false;
	
	// Global timer from "actors world"
	GetWorldTimerManager().SetTimer(closeDoorDelayHandle, this, &AFloorSwitch::closeDoor, closeDoorDelay);
}

//
void AFloorSwitch::updateDoorLocation(float z)
{
	FVector newLocation = initialDoorLocation;
	newLocation.Z += z;
	door->SetWorldLocation(newLocation);
}

void AFloorSwitch::updateFloorSwitchLocation(float z)
{
	FVector newLocation = initialSwitchLocation;
	newLocation.Z += z;
	floorSwitch->SetWorldLocation(newLocation);
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
// Called when the game starts or when spawned
void AFloorSwitch::BeginPlay()
{
	Super::BeginPlay();

	triggerBox->OnComponentBeginOverlap.AddDynamic(this, &AFloorSwitch::onOverlapBegin);
	triggerBox->OnComponentEndOverlap.AddDynamic(this, &AFloorSwitch::onOverlapEnd);

	initialDoorLocation = door->GetComponentLocation();
	initialSwitchLocation = floorSwitch->GetComponentLocation();
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
void AFloorSwitch::closeDoor()
{
	if(!characterOnFloorSwitch)
	{
		lowerDoor();
		raiseFloorSwitch();
	}
}
