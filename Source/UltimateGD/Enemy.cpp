// Fill out your copyright notice in the Description page of Project Settings.
#include "Enemy.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Animation/AnimInstance.h"
#include "TimerManager.h"
#include "MainPlayerController.h"
#include "Components/CapsuleComponent.h"


// Sets default values
AEnemy::AEnemy()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// ======================================== Movement
	movementAcceptanceRadius = 10.f;
	movementState = EnemyMovementState::Idle;

	// ======================================== Collision / Combat
	agroSphere = CreateDefaultSubobject<USphereComponent>(TEXT("agroSphere"));
	agroSphere->SetupAttachment(GetRootComponent());
	agroSphere->InitSphereRadius(600.f);

	combatSphere = CreateDefaultSubobject<USphereComponent>(TEXT("combatSphere"));
	combatSphere->SetupAttachment(GetRootComponent());
	combatSphere->InitSphereRadius(75.f);

	attackCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("attackCollision"));
	// Add attack/damage collision box to socket in enemy (Spider) left claw
	//attackCollision->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, TEXT("EnemySocket"));
	attackCollision->SetupAttachment(GetMesh(), FName("EnemySocket"));


	overlappingCombatSphere = false;

	// ======================================== Enemy Stats
	maxHealth = 100.f;
	health = 75.f;

	damage = 10.f;

	// ======================================== Attack
	attacking = false;
	attackAnimationSpeed = 1.35;

	// ======================================== Attack delay
	attackMinDelay = 0.5f;
	attackMaxDelay = 2.f;

	// ======================================== Death + Disappear
	disappearDelay = 3.f;

	// ======================================== Sounds
	hitSoundVolume = 1.f;
	swingSoundVolume = 1.f;
}

//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// ======================================== Enemy Stats
void AEnemy::increaseHealth(float value)
{
	health += value;
	if (health > maxHealth)
		health = maxHealth;
}

void AEnemy::decreaseHealth(float value)
{
	health -= value;
	if (health < 0)
		health = 0;
}

bool AEnemy::alive()
{
	return movementState != EnemyMovementState::Dead;
}

// ======================================== Collision / Combat
// Agro radius
void AEnemy::onAgroOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                UPrimitiveComponent *OtherComp,
                                int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("AEnemy::onAgroOverlapBegin"));
	if (OtherActor)
	{
		AMainCharacter *mainCharacter = Cast<AMainCharacter>(OtherActor);
		if (mainCharacter)
		{
			combatTarget = mainCharacter; // in v. lessons we set it in onCombatOverlapBegin() but ...
			// but this variable used only to FOLLOW target.
			moveToTarget(mainCharacter);
		}
	}
}

void AEnemy::onAgroOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                              UPrimitiveComponent *OtherComp,
                              int32 OtherBodyIndex)
{
	//UE_LOG(LogTemp, Warning, TEXT("AEnemy::onAgroOverlapEnd"));
	if (OtherActor)
	{
		AMainCharacter *mainCharacter = Cast<AMainCharacter>(OtherActor);
		if (mainCharacter)
		{
			combatTarget = nullptr;
			setEnemyMovementState(EnemyMovementState::Idle);
			if (aiController)
			{
				aiController->StopMovement();
			}

			//
			if (mainCharacter->attackTarget == this)
			{
				// because player can have multiple targets
				mainCharacter->setAttackTarget(nullptr); // Done like in v. lessons...
				// Done like in v. lessons...
				if (mainCharacter->mainPlayerController)
					mainCharacter->mainPlayerController->hideEnemyHealthBar();
			}
		}
	}
}

// Combat radius
void AEnemy::onCombatOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                  UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                  const FHitResult &SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("AEnemy::onCombatOverlapBegin"));s
	if (OtherActor)
	{
		AMainCharacter *mainCharacter = Cast<AMainCharacter>(OtherActor);
		if (mainCharacter)
		{
			overlappingCombatSphere = true;
			attack();

			// 
			/*if (combatTarget->attackTarget == nullptr)
			{
				combatTarget->setAttackTarget(this); // WHY??? We must set it in MainCharacter, when enemy in ...
				if (combatTarget->mainPlayerController)
					combatTarget->mainPlayerController->displayEnemyHealthBar();
				// ... in MainCharacter attackRadius. Done like in v. lessons...
			}*/
		}
	}
}

void AEnemy::onCombatOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
{
	//UE_LOG(LogTemp, Warning, TEXT("AEnemy::onCombatOverlapEnd"));
	if (OtherActor)
	{
		AMainCharacter *mainCharacter = Cast<AMainCharacter>(OtherActor);
		if (mainCharacter)
		{
			overlappingCombatSphere = false;
			// Note: follow player again, onEndAttack() will be called from SpiderAnim_BP
		}
	}
}

// ======================================== Attack
void AEnemy::attack()
{
	// ==================================================
	if (GetWorldTimerManager().IsTimerActive(attackDelayHandle) && GetWorldTimerManager().GetTimerRemaining(
		attackDelayHandle) > 0)
	{
		// Don't interrupt delay before next attack
		return;
	}
	GetWorldTimerManager().ClearTimer(attackDelayHandle); // v. lesson line was in onCombatOverlapEnd()

	// ==================================================
	if (aiController && !attacking && overlappingCombatSphere)
	{
		// ======================================== Case when 2 >= enemies attack player, and one of them become killed
		// Because mainCharacter->updateAttackTarget() doesn't work properly (done like in v. lessons)
		if (combatTarget->attackTarget == nullptr)
		{
			combatTarget->setAttackTarget(this); // WHY??? We must set it in MainCharacter, when enemy in ...
			if (combatTarget->mainPlayerController)
				combatTarget->mainPlayerController->displayEnemyHealthBar();
			// ... in MainCharacter attackRadius. Done like in v. lessons...
		}

		// ==================================================
		aiController->StopMovement();

		attacking = true;
		setEnemyMovementState(EnemyMovementState::Attacking);

		UAnimInstance *animInstance = GetMesh()->GetAnimInstance();
		if (animInstance && combatMontage)
		{
			animInstance->Montage_Play(combatMontage, attackAnimationSpeed);
			animInstance->Montage_JumpToSection(FName("Attack"), combatMontage);
		}
	}
	else // No such condition in v. lessons.
	{
		// Try follow target after delay between attack
		tryFollowTarget();
	}
}

void AEnemy::attackEnd()
{
	attacking = false;

	//if (overlappingCombatSphere)
	{
		// Delay before attack OR before following the player
		float attackDelay = FMath::RandRange(attackMinDelay, attackMaxDelay);
		GetWorldTimerManager().SetTimer(attackDelayHandle, this, &AEnemy::attack, attackDelay);
	}
	/*else
	{
		tryFollowTarget();
	}*/
}

void AEnemy::activateCollision()
{
	// Collision only for: overlap, raycast, etc. No physics collision
	attackCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AEnemy::deactivateCollision()
{
	attackCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AEnemy::playSwingSound()
{
	if (swingSound)
	{
		UGameplayStatics::PlaySound2D(this, swingSound, swingSoundVolume);
	}
}

void AEnemy::onAttackOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                  UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                  const FHitResult &SweepResult)
{
	if (OtherActor)
	{
		AMainCharacter *mainCharacter = Cast<AMainCharacter>(OtherActor);
		if (mainCharacter && damageTypeClass)
		{
			//mainCharacter->takeDamage(damage, socketLocation);
			// How difficult to simply deal damage... :)
			UGameplayStatics::ApplyDamage(mainCharacter, damage, aiController, this, damageTypeClass);
			// Note: above will call mainPlayer->TakeDamage()


			/*const USkeletalMeshSocket *clawTipSocket = GetMesh()->GetSocketByName("TipSocket");
			if (clawTipSocket)
			{
				FVector socketLocation = clawTipSocket->GetSocketLocation(GetMesh());
			}*/
		}
	}
}

void AEnemy::onAttackOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
                                UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
{
	//UE_LOG(LogTemp, Warning, TEXT("AWeapon::onCombatOverlapEnd()"));
}

// ======================================== Take damage
float AEnemy::TakeDamage(float Damage, FDamageEvent const &DamageEvent, AController *EventInstigator,
                         AActor *DamageCauser)
{
	decreaseHealth(Damage);
	if (health == 0)
		die(DamageCauser);
	//UE_LOG(LogTemp, Warning, TEXT("Current health: %f"), health);

	if (hitParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), hitParticles, GetActorLocation(), FRotator(0.f), true,
		                                         EPSCPoolMethod::None);
		// Note: EPSCPoolMethod - Enum ParticlesSystemComponent ...
	}
	if (hitSound)
	{
		UGameplayStatics::PlaySound2D(this, hitSound, hitSoundVolume);
	}

	return Damage;
}

// ======================================== Death + Disappear
void AEnemy::die(AActor *causer)
{
	setEnemyMovementState(EnemyMovementState::Dead); // Perfect movement state (sarcasm)
	GetWorldTimerManager().ClearTimer(attackDelayHandle); // v. lesson line was in onCombatOverlapEnd()

	// Death animation
	UAnimInstance *animInstance = GetMesh()->GetAnimInstance();
	if (animInstance && combatMontage)
	{
		animInstance->Montage_Play(combatMontage, 1.f);
		animInstance->Montage_JumpToSection(FName("Death"), combatMontage);
		// Note: I think this isn't the best place to store death animation (I mean why with attacks?)
	}

	// Disable collisions
	attackCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	agroSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	combatSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	// Update attackTarget for mainCharacter (done like in v. lessons)
	// But, this must be done in mainCharacter ...
	AMainCharacter *mainCharacter = Cast<AMainCharacter>(causer);
	if (mainCharacter)
	{
		mainCharacter->updateAttackTarget();
	}
}

void AEnemy::deathEnd()
{
	GetMesh()->bPauseAnims = true;
	GetMesh()->bNoSkeletonUpdate = true;

	GetWorldTimerManager().SetTimer(disapearDelayHandle, this, &AEnemy::disappear, disappearDelay);
}

void AEnemy::disappear()
{
	Destroy();
}

// ======================================== Common
// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	// Make sure not to ZOOM when camera between player and enemy
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	//agroSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	//combatSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);

	// ======================================== Movement
	aiController = Cast<AAIController>(GetController());

	// ======================================== Collision / Combat
	// Agro radius
	agroSphere->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::onAgroOverlapBegin);
	agroSphere->OnComponentEndOverlap.AddDynamic(this, &AEnemy::onAgroOverlapEnd);

	// Combat radius
	combatSphere->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::onCombatOverlapBegin);
	combatSphere->OnComponentEndOverlap.AddDynamic(this, &AEnemy::onCombatOverlapEnd);

	// ======================================== Attack
	attackCollision->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::onAttackOverlapBegin);
	attackCollision->OnComponentEndOverlap.AddDynamic(this, &AEnemy::onAttackOverlapEnd);

	// Collision will be activated during attack animation (on "Notify")
	attackCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision); // will be activated during attack
	attackCollision->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	// Set collision response only for 1 specific chanel
	attackCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	attackCollision->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
void AEnemy::moveToTarget(AMainCharacter *target)
{
	setEnemyMovementState(EnemyMovementState::MoveToTarget);

	if (aiController)
	{
		//UE_LOG(LogTemp, Warning, TEXT("MoveToTarget()"));

		// ======================================== MoveTo() player
		// Prepare parameters for MoveTo()
		FAIMoveRequest moveRequest;
		moveRequest.SetGoalActor(target);
		moveRequest.SetAcceptanceRadius(movementAcceptanceRadius);

		FNavPathSharedPtr navPath;

		//
		aiController->MoveTo(moveRequest, &navPath);
		// Note: second parameter called "OutPath".
		// This means, that we just need to pass object there, and function will set it's value automatically
		// (According to v. lessons author)

		// ======================================== Debug stuff (show enemy path (few points) to player)
		/*TArray<FNavPathPoint> pathPoints = navPath->GetPathPoints();
		for (auto point : pathPoints)
		{
			FVector location = point.Location;

			UKismetSystemLibrary::DrawDebugSphere(this, location, 25.f, 24,
			                                      FLinearColor::Green, 10.f, .5f);
		}*/
	}
}

// TODO: fix bug, when enemy reached player, but there is no attackSphere overlap, so enemy just look idle
// So, on MoveTo() reached player event (need to find something like that) - follow again if NOT attacking

void AEnemy::tryFollowTarget()
{
	// This part of code in v. lessons in BP (without any reason)
	// ... and with buggy conditions and changing variables
	if (!overlappingCombatSphere && combatTarget && !attacking)
	{
		moveToTarget(combatTarget);
	}
}
