// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Components/MeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Particles/ParticleSystem.h"
#include "Sound/SoundCue.h"
#include "Item.generated.h"


UCLASS()
class ULTIMATEGD_API AItem : public AActor
{
	GENERATED_BODY()

public:
	AItem();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item | Collision")
	USphereComponent *collisionVolume;
	// Note: "|" sign will put "Collision" to sub-category of "Item"

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item | Mesh")
	UStaticMeshComponent *mesh;

	// ======================================== Particles
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Particles")
	UParticleSystemComponent *idleParticles;

	/**
	 * This particles will be shown onOverlapBegin()
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Particles")
	UParticleSystem *overlapParticlesSystem;
	/**
	 * Sound will play onOverlapBegin()
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Sounds")
	USoundCue *overlapSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Sounds")
	float overlapSoundVolume;
	
	// ======================================== Rotation
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | ItemProperties")
	bool rotate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | ItemProperties")
	float rotationRate;

	// ==================================================
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//
	UFUNCTION() // <--- this is MUST, for overlap functions
	virtual void onOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                            UPrimitiveComponent *OtherComp,
	                            int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION() // <--- this is MUST, for overlap functions
	virtual void onOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                          UPrimitiveComponent *OtherComp,
	                          int32 OtherBodyIndex);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
