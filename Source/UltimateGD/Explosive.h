// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"
#include "Item.h"
#include "Explosive.generated.h"


/**
 * 
 */
UCLASS()
class ULTIMATEGD_API AExplosive : public AItem
{
	GENERATED_BODY()

public:
	AExplosive();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	float damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	TSubclassOf<UDamageType> damageTypeClass; // some UE special way for dealing damage
	// with "TSubclassOf<UUserWidget>" - we have type check during compilation, so wrong type can't be passed
	// we can use "UClass*" instead - in that case type check will be performed during runtime, which is good way to have bugs
	
	// ==================================================
	//UFUNCTION() // <--- no need for this here, since it's already in parent (uncomment will give ERROR)
	virtual void onOverlapBegin(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                            UPrimitiveComponent *OtherComp,
	                            int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult) override;

	//UFUNCTION() // <--- no need for this here, since it's already in parent (uncomment will give ERROR)
	virtual void onOverlapEnd(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor,
	                          UPrimitiveComponent *OtherComp,
	                          int32 OtherBodyIndex) override;
};
