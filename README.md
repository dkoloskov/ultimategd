Project is based on the course: Unreal Engine C++ The Ultimate Game Developer Course  
https://www.udemy.com/course/unreal-engine-the-ultimate-game-developer-course/  
  
// ======================== CONTROLS ========================  
= Keyboard =  
	Movement: WASD  
	Jump: Space  
	Rotate camera: Arrows, Mouse  
	Sprint: Shift + Movement  
	Show picked up coins locations: N  
	Pickup weapon / Attack: LMB  
	-----  
	Save game: 5  
	Load game: 6  
	Pause menu: Q/Esc  
= Gamepad =  
	Movement: Left Thumbstick  
	Jump: A button  
	Rotate camera: Right Thumbstick  
	Sprint: Right Trigger + Movement  
	Show picked up coins locations: ? (unasigned)  
	Pickup weapon / Attack: X button  
	-----  
	Save game: ? (unasigned)  
	Load game: ? (unasigned)  
	Pause menu: ? (unasigned)  
  
// ======================== PROJECT SETUP ========================  
Note: This content is in .gitignore, so must be downloaded from the marketplace:   
	- StarterContent   
	- InfinityBladeAdversaries  
		> Also create socket in npc skeleton:  
		  SK_Greater_Spider:Front_Left_Knee - create "EnemySocket"  
		  Otherwise enemy wouldn't be able to attack (worst case - couldn't compile game)  
	- Infinity Blade: Effects  
	- Infinity Blade: Weapons  
	- Infinity Blade: Grass Lands  
	- ParagonMinions  
		> Also create socket in npc skeleton:  
		  Minion_Lane_Core_Skeleton:weapon_sword_l - create "EnemySocket"  
		  Otherwise enemy wouldn't be able to attack (worst case - couldn't compile game)  
	- SunTemple (Epic Games => Learn)  
	  Note: actually "SunTemple" map was migrated to this project in few steps:  
		> Renamed folder "Assets" => "SunTemple"  
		> Moved "Blueprints" folder to "SunTemple"  
  
// ======================== Known issues (Things that can be improved) ========================  
	GAMEPLAY (most critical stuff)  
	- No functionality for transfer the weapon or character stats to new level (not a problem to implement, but I don't want to invest time for this right now)  
	- Enemy is continues to attack event when player behind (just need one method to interpolate to player direction - easy).  
	- After loading player continues to move (fun bug, don't want to fix it)  
	- Create Main menu, with options:  
		> New game  
			* Choose slot to save (+ maybe: write profile/player name)  
		> Load game  
			* Choose slot to load  
		> Quit  
	  
	SOURCE CODE  
	- Move behavior logic to "State Machine" (I think about classic pattern) from MainCharacter, Enemy, Weapon classes.  
	- Move game functionality like: save/loadGame(), switchLevel(), etc to ...   
	  ... to: GameMode or GameInstance (or other class, need to think more about proper refactoring.)  
	- Move Enum classes to separate files, from MainCharacter and Enemy.  
	  
	Note: I tried to follow video lessons, so many stuff implemented not in the best way, but, I know how to do it better!  
	      Changing it during v. lessons will make harder to proceed with following lessons (been there, done that)  
	Note 2: Things above isn't hard to implement, but I don't want to spend more time polishing this project.  
  
// ======================== REDUNDANT CLASSES ========================  
Redundant classes, which I haven't deleted to serve as examples:  
	- MyObject (test class)  
	- Floater (test class)  
	- Collider (test class, implementation of Character functionality on Pawn. Replaced with Creature)  
	- Creature (test class. Replaced with Enemy)  